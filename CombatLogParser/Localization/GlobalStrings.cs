﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.Localization
{
    public enum GlobalString
    {
        ERR_NOT_WHILE_DISARMED,
        SPELL_FAILED_DAMAGE_IMMUNE,
        SPELL_FAILED_DISABLED_BY_AURA_LABEL,
        SPELL_FAILED_DISABLED_BY_MAX_USABLE_LEVEL,
        SPELL_FAILED_DISABLED_BY_POWER_SCALING,
        SPELL_FAILED_ERROR,
        SPELL_FAILED_FALLING,
        SPELL_FAILED_FIZZLE,
        SPELL_FAILED_FLEEING,
        SPELL_FAILED_HIGHLEVEL,
        SPELL_FAILED_IMMUNE,
        SPELL_FAILED_INCORRECT_AREA,
        SPELL_FAILED_INTERRUPTED,
        SPELL_FAILED_ITEM_NOT_READY,
        SPELL_FAILED_LINE_OF_SIGHT,
        SPELL_FAILED_LOWLEVEL,
        SPELL_FAILED_LOW_CASTLEVEL,
        SPELL_FAILED_MAINHAND_EMPTY,
        SPELL_FAILED_MOVING,
        SPELL_FAILED_MUST_BE_ON_GROUND,
        SPELL_FAILED_NOPATH,
        SPELL_FAILED_NOTHING_TO_DISPEL,
        SPELL_FAILED_NOT_BEHIND,
        SPELL_FAILED_NOT_FLYING,
        SPELL_FAILED_NOT_HERE,
        SPELL_FAILED_NOT_IDLE,
        SPELL_FAILED_NOT_INFRONT,
        SPELL_FAILED_NOT_IN_ARENA,
        SPELL_FAILED_NOT_IN_CONTROL,
        SPELL_FAILED_NOT_KNOWN,
        SPELL_FAILED_NOT_MOUNTED,
        SPELL_FAILED_NOT_ON_DAMAGE_IMMUNE,
        SPELL_FAILED_NOT_ON_GROUND,
        SPELL_FAILED_NOT_ON_MOUNTED,
        SPELL_FAILED_NOT_ON_SHAPESHIFT,
        SPELL_FAILED_NOT_ON_STEALTHED,
        SPELL_FAILED_NOT_READY,
        SPELL_FAILED_NOT_SHAPESHIFT,
        SPELL_FAILED_NO_ACTIONS,
        SPELL_FAILED_NO_BONE_SHIELD,
        SPELL_FAILED_NO_CHARGES_REMAIN,
        SPELL_FAILED_NO_COMBO_POINTS,
        SPELL_FAILED_NO_EDIBLE_CORPSES,
        SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED,
        SPELL_FAILED_NO_MAGIC_TO_CONSUME,
        SPELL_FAILED_NO_PET,
        SPELL_FAILED_ONLY_BATTLEGROUNDS,
        SPELL_FAILED_ONLY_STEALTHED,
        SPELL_FAILED_OUT_OF_RANGE,
        SPELL_FAILED_PACIFIED,
        SPELL_FAILED_POSSESSED,
        SPELL_FAILED_ROOTED,
        SPELL_FAILED_SILENCED,
        SPELL_FAILED_SPELL_IN_PROGRESS,
        SPELL_FAILED_SPELL_UNAVAILABLE,
        SPELL_FAILED_SPELL_UNAVAILABLE_PET,
        SPELL_FAILED_STUNNED,
        SPELL_FAILED_TARGETS_DEAD,
        SPELL_FAILED_TARGET_AFFECTING_COMBAT,
        SPELL_FAILED_TARGET_AURASTATE,
        SPELL_FAILED_TARGET_CANNOT_BE_RESURRECTED,
        SPELL_FAILED_TARGET_ENEMY,
        SPELL_FAILED_TARGET_ENRAGED,
        SPELL_FAILED_TARGET_FRIENDLY,
        SPELL_FAILED_TOO_CLOSE,
        SPELL_FAILED_TRY_AGAIN,
        SPELL_FAILED_UNIT_NOT_BEHIND,
        SPELL_FAILED_UNIT_NOT_INFRONT,
        SPELL_FAILED_UNKNOWN,
        SPELL_FAILED_VISION_OBSCURED,
        SPELL_FAILED_YOU_CANNOT_USE_THAT_IN_PVP_INSTANCE,
        SPELL_FAILED_BAD_TARGETS,
        SPELL_FAILED_PREVENTED_BY_MECHANIC,
        SPELL_FAILED_CASTER_DEAD,
        SPELL_FAILED_CASTER_DEAD_FEMALE,
        SPELL_FAILED_BAD_IMPLICIT_TARGETS,
        UNKNOWN_GLOBAL_STRING,
    }
}
