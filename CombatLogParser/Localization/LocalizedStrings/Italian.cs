﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.Localization.LocalizedStrings
{
    public static class Italian
    {
        public static readonly Dictionary<GlobalString, string> LocalizedStrings =
            new Dictionary<GlobalString, string>
            {
                { GlobalString.ERR_NOT_WHILE_DISARMED,                              "Non puoi farlo quando sei disarmato." },
                { GlobalString.SPELL_FAILED_DAMAGE_IMMUNE,                          "Non puoi farlo mentre sei immune." },
                { GlobalString.SPELL_FAILED_DISABLED_BY_AURA_LABEL,                 "Un'aura sta impendendo l'uso di questo incantesimo" },
                { GlobalString.SPELL_FAILED_DISABLED_BY_MAX_USABLE_LEVEL,           "Il tuo livello è superiore al livello massimo consentito per usare questo incantesimo." },
                { GlobalString.SPELL_FAILED_DISABLED_BY_POWER_SCALING,              "Non è possibile farlo al tuo livello." },
                { GlobalString.SPELL_FAILED_ERROR,                                  "Errore interno." },
                { GlobalString.SPELL_FAILED_FALLING,                                "Non puoi farlo quando stai cadendo." },
                { GlobalString.SPELL_FAILED_FIZZLE,                                 "Incantesimo fallito." },
                { GlobalString.SPELL_FAILED_FLEEING,                                "Non puoi farlo mentre sei in fuga." },
                { GlobalString.SPELL_FAILED_HIGHLEVEL,                              "Il bersaglio è di livello troppo alto." },
                { GlobalString.SPELL_FAILED_IMMUNE,                                 "Immune" },
                { GlobalString.SPELL_FAILED_INCORRECT_AREA,                         "Sei nella zona sbagliata!" },
                { GlobalString.SPELL_FAILED_INTERRUPTED,                            "Interrotto" },
                { GlobalString.SPELL_FAILED_ITEM_NOT_READY,                         "L'oggetto non è pronto." },
                { GlobalString.SPELL_FAILED_LINE_OF_SIGHT,                          "Bersaglio fuori dal campo visivo." },
                { GlobalString.SPELL_FAILED_LOWLEVEL,                               "Il bersaglio è di livello troppo basso." },
                { GlobalString.SPELL_FAILED_MAINHAND_EMPTY,                         "Non hai armi in mano." },
                { GlobalString.SPELL_FAILED_MOVING,                                 "Non puoi farlo in movimento." },
                { GlobalString.SPELL_FAILED_MUST_BE_ON_GROUND,                      "Devi essere a terra." },
                { GlobalString.SPELL_FAILED_NOPATH,                                 "Nessun percorso disponibile." },
                { GlobalString.SPELL_FAILED_NOTHING_TO_DISPEL,                      "Nulla da dissolvere." },
                { GlobalString.SPELL_FAILED_NOT_BEHIND,                             "Devi essere dietro al tuo bersaglio." },
                { GlobalString.SPELL_FAILED_NOT_FLYING,                             "Stai volando." },
                { GlobalString.SPELL_FAILED_NOT_HERE,                               "Qui non puoi utilizzarlo." },
                { GlobalString.SPELL_FAILED_NOT_IDLE,                               "Non si può utilizzare quando sei inattivo." },
                { GlobalString.SPELL_FAILED_NOT_INFRONT,                            "Devi essere davanti al tuo bersaglio." },
                { GlobalString.SPELL_FAILED_NOT_IN_ARENA,                           "Non puoi farlo in arena." },
                { GlobalString.SPELL_FAILED_NOT_IN_CONTROL,                         "Non hai il controllo delle tue azioni" },
                { GlobalString.SPELL_FAILED_NOT_KNOWN,                              "Incantesimo non appreso." },
                { GlobalString.SPELL_FAILED_NOT_MOUNTED,                            "Sei su una cavalcatura." },
                { GlobalString.SPELL_FAILED_NOT_ON_DAMAGE_IMMUNE,                   "Questo incantesimo non può essere lanciato su un bersaglio immune." },
                { GlobalString.SPELL_FAILED_NOT_ON_GROUND,                          "Non puoi usarlo a terra." },
                { GlobalString.SPELL_FAILED_NOT_ON_MOUNTED,                         "Questo incantesimo non può essere lanciato su un bersaglio su una cavalcatura." },
                { GlobalString.SPELL_FAILED_NOT_ON_SHAPESHIFT,                      "Questo incantesimo non può essere lanciato su un bersaglio che ha mutato forma." },
                { GlobalString.SPELL_FAILED_NOT_ON_STEALTHED,                       "Questo incantesimo non può essere lanciato su un bersaglio nascosto." },
                { GlobalString.SPELL_FAILED_NOT_READY,                              "Non ti sei ancora ripreso." },
                { GlobalString.SPELL_FAILED_NOT_SHAPESHIFT,                         "Non puoi farlo in questa forma." },
                { GlobalString.SPELL_FAILED_NO_ACTIONS,                             "Non puoi eseguire azioni in questo momento." },
                { GlobalString.SPELL_FAILED_NO_BONE_SHIELD,                         "Richiede delle cariche di Scudo d'Ossa." },
                { GlobalString.SPELL_FAILED_NO_CHARGES_REMAIN,                      "Cariche esaurite." },
                { GlobalString.SPELL_FAILED_NO_COMBO_POINTS,                        "Questa abilità richiede punti combo." },
                { GlobalString.SPELL_FAILED_NO_EDIBLE_CORPSES,                      "Non ci sono cadaveri da mangiare nelle vicinanze." },
                { GlobalString.SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED,            "Non puoi usare oggetti se hai mutato forma." },
                { GlobalString.SPELL_FAILED_NO_MAGIC_TO_CONSUME,                    "Nessuna magia da consumare." },
                { GlobalString.SPELL_FAILED_NO_PET,                                 "Non hai un famiglio." },
                { GlobalString.SPELL_FAILED_ONLY_BATTLEGROUNDS,                     "Può essere usato solo nei campi di battaglia." },
                { GlobalString.SPELL_FAILED_ONLY_STEALTHED,                         "Devi essere in modalità furtiva." },
                { GlobalString.SPELL_FAILED_OUT_OF_RANGE,                           "Fuori portata" },
                { GlobalString.SPELL_FAILED_PACIFIED,                               "Non puoi utilizzare questa abilità quando sei pacificato." },
                { GlobalString.SPELL_FAILED_POSSESSED,                              "Sei posseduto" },
                { GlobalString.SPELL_FAILED_ROOTED,                                 "Non puoi muoverti." },
                { GlobalString.SPELL_FAILED_SILENCED,                               "Non puoi farlo quando sei silenziato." },
                { GlobalString.SPELL_FAILED_SPELL_IN_PROGRESS,                      "Altra azione in corso." },
                { GlobalString.SPELL_FAILED_SPELL_UNAVAILABLE,                      "Questo incantesimo non è disponibile per te." },
                { GlobalString.SPELL_FAILED_SPELL_UNAVAILABLE_PET,                  "Questa abilità non è disponibile per il tuo famiglio." },
                { GlobalString.SPELL_FAILED_STUNNED,                                "Non puoi farlo quando sei stordito." },
                { GlobalString.SPELL_FAILED_TARGETS_DEAD,                           "Il bersaglio è morto." },
                { GlobalString.SPELL_FAILED_TARGET_AFFECTING_COMBAT,                "Il bersaglio è in combattimento." },
                { GlobalString.SPELL_FAILED_TARGET_AURASTATE,                       "In questo momento non puoi farlo." },
                { GlobalString.SPELL_FAILED_TARGET_CANNOT_BE_RESURRECTED,           "Il bersaglio non può essere resuscitato." },
                { GlobalString.SPELL_FAILED_TARGET_ENEMY,                           "Il bersaglio è ostile." },
                { GlobalString.SPELL_FAILED_TARGET_ENRAGED,                         "Il bersaglio è troppo infuriato per poter essere ammaliato." },
                { GlobalString.SPELL_FAILED_TARGET_FRIENDLY,                        "Il bersaglio è amico." },
                { GlobalString.SPELL_FAILED_TOO_CLOSE,                              "Bersaglio troppo vicino." },
                { GlobalString.SPELL_FAILED_TRY_AGAIN,                              "Tentativo fallito." },
                { GlobalString.SPELL_FAILED_UNIT_NOT_BEHIND,                        "Il bersaglio deve essere dietro di te." },
                { GlobalString.SPELL_FAILED_UNIT_NOT_INFRONT,                       "Il bersaglio deve essere di fronte a te." },
                { GlobalString.SPELL_FAILED_UNKNOWN,                                "Ragione sconosciuta." },
                { GlobalString.SPELL_FAILED_VISION_OBSCURED,                        "La visuale del bersaglio è oscurata." },
                { GlobalString.SPELL_FAILED_YOU_CANNOT_USE_THAT_IN_PVP_INSTANCE,    "Non puoi utilizzare questo oggetto in un'istanza PvP." },
                { GlobalString.SPELL_FAILED_BAD_TARGETS,                            "Bersaglio non valido." },
                { GlobalString.SPELL_FAILED_PREVENTED_BY_MECHANIC,                  "Non puoi farlo quando sei" },
                { GlobalString.SPELL_FAILED_CASTER_DEAD,                            "Sei morto" },
                { GlobalString.SPELL_FAILED_CASTER_DEAD_FEMALE,                     "Sei morto" },
                { GlobalString.UNKNOWN_GLOBAL_STRING,                               "Unknown error" },
            };
    }
}
