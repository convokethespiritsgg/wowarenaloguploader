﻿using CombatLogParser.Localization.SpecialCases;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.Localization
{
     public class LocalizedArray
     {
        protected Dictionary<GlobalString, string> StringValues;
        protected Locale loc;
        public string GetByStringID(GlobalString id)
        {
            if (StringValues.ContainsKey(id))
                return StringValues[id];
            return StringValues[GlobalString.UNKNOWN_GLOBAL_STRING];
        }
        public string GetPreventedByMechanicString(string reason)
        {
            return SpellPreventedByMechanicParser.RecreateOriginal(reason, loc);
        }
        public GlobalString GetByStringValue(string str)
        {
            foreach(KeyValuePair<GlobalString, string> pair in StringValues)
            {
                if (str.Contains(pair.Value))
                    return pair.Key;
            }
            return GlobalString.UNKNOWN_GLOBAL_STRING;
        }
        public Locale GetLocale()
        {
            return loc;
        }

        public LocalizedArray(Locale loc, Dictionary<GlobalString, string> vals)
        {
            this.loc = loc;
            StringValues = new Dictionary<GlobalString, string>(vals);
        }
     }
}
