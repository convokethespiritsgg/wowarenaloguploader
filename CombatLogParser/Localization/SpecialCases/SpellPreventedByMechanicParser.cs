﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CombatLogParser.Localization.SpecialCases
{
    public static class SpellPreventedByMechanicParser
    {
        public static string Parse(string input, Locale loc)
        {
            string pattern = loc switch
            {
                Locale.deDE => @"Das geht nicht, während Euer Status '(.*)' ist\.",
                Locale.enUS => @"Can't do that while (.*)",
                Locale.esES => @"No puedes hacer eso mientras estás (.*)",
                Locale.esMX => @"No puedes hacer eso mientras estás (.*)",
                Locale.frFR => @"Impossible lorsque vous êtes (.*)",
                Locale.itIT => @"Non puoi farlo quando sei (.*)\.",
                Locale.ptBR => @"Você não pode fazer isto enquanto estiver (.*)",
                Locale.ruRU => @"Действие невозможно. Причина: (.*)\.",
                Locale.koKR => @"(.*) 중에는 불가능합니다\.",
                Locale.zhCN => @"无法在(.*)时那样做",
                Locale.zhTW => @"無法在(.*)狀態下這麼作",
                _ => @"Unknown",
            };
            Match m = Regex.Match(input, pattern, RegexOptions.IgnoreCase);
            if (m.Success && m.Groups.Count >= 2)
                return m.Groups[1].ToString();

            return "Unknown";
        }

        public static string RecreateOriginal(string reason, Locale loc)
        {
            string pattern = loc switch
            {
                Locale.deDE => "Das geht nicht, während Euer Status '###' ist.",
                Locale.enUS => "Can't do that while ###",
                Locale.esES => "No puedes hacer eso mientras estás ###",
                Locale.esMX => "No puedes hacer eso mientras estás ###",
                Locale.frFR => "Impossible lorsque vous êtes ###",
                Locale.itIT => "Non puoi farlo quando sei ###.",
                Locale.ptBR => "Você não pode fazer isto enquanto estiver ###",
                Locale.ruRU => "Действие невозможно. Причина: ###.",
                Locale.koKR => "### 중에는 불가능합니다.",
                Locale.zhCN => "无法在###时那样做",
                Locale.zhTW => "無法在###狀態下這麼作",
                _ => "Unknown: ###",
            };

            return pattern.Replace("###", reason);
        }
    }
}
