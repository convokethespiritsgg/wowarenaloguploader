﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.Localization
{
    public enum Locale
    {
        deDE,
        enUS,
        esES,
        esMX,
        frFR,
        itIT,
        ptBR,
        ruRU,
        koKR,
        zhCN,
        zhTW
    }

    public static class LocaleString
    {
        public static Locale Parse(string locstr)
        {
            return locstr switch
            {
                "deDE" => Locale.deDE,
                "enUS" => Locale.enUS,
                "esES" => Locale.esES,
                "esMX" => Locale.esMX,
                "frFR" => Locale.frFR,
                "itIT" => Locale.itIT,
                "ptBR" => Locale.ptBR,
                "ruRU" => Locale.ruRU,
                "koKR" => Locale.koKR,
                "zhCN" => Locale.zhCN,
                "zhTW" => Locale.zhTW,
                _ => Locale.enUS
            };
        }
    }
}
