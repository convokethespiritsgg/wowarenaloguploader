﻿using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace CombatLogParser
{
    public class JsonSerializerConfig
    {
        public static readonly JsonSerializerOptions opts = new JsonSerializerOptions() {
            Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All),
            IgnoreNullValues = true
        };
    }

    public class JsonCombatLogSerializer
    {
        public static string SerealizeCollection<T>(IEnumerable<T> Elements)
        {
            StringBuilder sb = new StringBuilder();

            int i = 0;
            for (var it = Elements.GetEnumerator(); it.MoveNext(); i++)
            {
                sb.Append(JsonSerializer.Serialize(it.Current, it.Current.GetType(), JsonSerializerConfig.opts));
                if (i != Elements.Count() - 1)
                    sb.Append(",");
            }

            return sb.ToString();
        }

        public static List<BaseLogEntry> DeserializeLogEntryCollection(JsonElement collection)
        {
            List<BaseLogEntry> eventList = new List<BaseLogEntry>();

            foreach (var e in collection.EnumerateArray())
            {
                LogEntryType type = (LogEntryType)Enum.ToObject(typeof(LogEntryType), e.GetProperty("Type").GetInt32());
                BaseLogEntry entry = DeserializeLogEntry(type, e.ToString());
                eventList.Add(entry);
            }

            return eventList;
        }

        public static List<T> DeserializeCollection<T>(JsonElement collection)
        {
            List<T> list = new List<T>();

            foreach (var e in collection.EnumerateArray())
            {
                T entry = JsonSerializer.Deserialize<T>(e.ToString());
                list.Add(entry);
            }
            return list;
        }

        public static BaseLogEntry DeserializeLogEntry(LogEntryType enum_type, string e)
        {
            return enum_type switch
            {
                LogEntryType.ARENA_MATCH_END => JsonSerializer.Deserialize<ArenaMatchEnd>(e),
                LogEntryType.ARENA_MATCH_START => JsonSerializer.Deserialize<ArenaMatchStart>(e),
                LogEntryType.COMBAT_LOG_VERSION => JsonSerializer.Deserialize<CombatLogVersion>(e),
                LogEntryType.COMBATANT_INFO => JsonSerializer.Deserialize<CombatantInfo>(e),
                LogEntryType.DAMAGE_SPLIT => JsonSerializer.Deserialize<DamageSplit>(e),
                LogEntryType.ENVIRONMENTAL_DAMAGE => JsonSerializer.Deserialize<EnvironmentalDamage>(e),
                LogEntryType.PARTY_KILL => JsonSerializer.Deserialize<PartyKill>(e),
                LogEntryType.RANGE_DAMAGE => JsonSerializer.Deserialize<RangeDamage>(e),
                LogEntryType.RANGE_MISSED => JsonSerializer.Deserialize<RangeMissed>(e),
                LogEntryType.SPELL_ABSORBED => JsonSerializer.Deserialize<SpellAbsorbed>(e),
                LogEntryType.SPELL_AURA_APPLIED => JsonSerializer.Deserialize<SpellAuraApplied>(e),
                LogEntryType.SPELL_AURA_APPLIED_DOSE => JsonSerializer.Deserialize<SpellAuraAppliedDose>(e),
                LogEntryType.SPELL_AURA_BROKEN => JsonSerializer.Deserialize<SpellAuraBroken>(e),
                LogEntryType.SPELL_AURA_BROKEN_SPELL => JsonSerializer.Deserialize<SpellAuraBrokenSpell>(e),
                LogEntryType.SPELL_AURA_REFRESH => JsonSerializer.Deserialize<SpellAuraRefresh>(e),
                LogEntryType.SPELL_AURA_REMOVED => JsonSerializer.Deserialize<SpellAuraRemoved>(e),
                LogEntryType.SPELL_AURA_REMOVED_DOSE => JsonSerializer.Deserialize<SpellAuraRemovedDose>(e),
                LogEntryType.SPELL_CAST_FAILED => JsonSerializer.Deserialize<SpellCastFailed>(e),
                LogEntryType.SPELL_CAST_START => JsonSerializer.Deserialize<SpellCastStart>(e),
                LogEntryType.SPELL_CAST_SUCCESS => JsonSerializer.Deserialize<SpellCastSuccess>(e),
                LogEntryType.SPELL_CREATE => JsonSerializer.Deserialize<SpellCreate>(e),
                LogEntryType.SPELL_DAMAGE => JsonSerializer.Deserialize<SpellDamage>(e),
                LogEntryType.SPELL_DISPEL => JsonSerializer.Deserialize<SpellDispel>(e),
                LogEntryType.SPELL_DISPEL_FAILED => JsonSerializer.Deserialize<SpellDispelFailed>(e),
                LogEntryType.SPELL_DRAIN => JsonSerializer.Deserialize<SpellDrain>(e),
                LogEntryType.SPELL_ENERGIZE => JsonSerializer.Deserialize<SpellEnergize>(e),
                LogEntryType.SPELL_EXTRA_ATTACKS => JsonSerializer.Deserialize<SpellExtraAttacks>(e),
                LogEntryType.SPELL_HEAL => JsonSerializer.Deserialize<SpellHeal>(e),
                LogEntryType.SPELL_HEAL_ABSORBED => JsonSerializer.Deserialize<SpellHealAbsorbed>(e),
                LogEntryType.SPELL_INSTAKILL => JsonSerializer.Deserialize<SpellInstakill>(e),
                LogEntryType.SPELL_INTERRUPT => JsonSerializer.Deserialize<SpellInterrupt>(e),
                LogEntryType.SPELL_MISSED => JsonSerializer.Deserialize<SpellMissed>(e),
                LogEntryType.SPELL_PERIODIC_DAMAGE => JsonSerializer.Deserialize<SpellPeriodicDamage>(e),
                LogEntryType.SPELL_PERIODIC_ENERGIZE => JsonSerializer.Deserialize<SpellPeriodicEnergize>(e),
                LogEntryType.SPELL_PERIODIC_HEAL => JsonSerializer.Deserialize<SpellPeriodicHeal>(e),
                LogEntryType.SPELL_PERIODIC_MISSED => JsonSerializer.Deserialize<SpellPeriodicMissed>(e),
                LogEntryType.SPELL_STOLEN => JsonSerializer.Deserialize<SpellStolen>(e),
                LogEntryType.SPELL_SUMMON => JsonSerializer.Deserialize<SpellSummon>(e),
                LogEntryType.SWING_DAMAGE => JsonSerializer.Deserialize<SwingDamage>(e),
                LogEntryType.SWING_DAMAGE_LANDED => JsonSerializer.Deserialize<SwingDamageLanded>(e),
                LogEntryType.SWING_MISSED => JsonSerializer.Deserialize<SwingMissed>(e),
                LogEntryType.UNIT_DESTROYED => JsonSerializer.Deserialize<UnitDestroyed>(e),
                LogEntryType.UNIT_DIED => JsonSerializer.Deserialize<UnitDied>(e),
                LogEntryType.ZONE_CHANGE => JsonSerializer.Deserialize<ZoneChange>(e),
                LogEntryType.UNKNOWN => JsonSerializer.Deserialize<BaseLogEntry>(e),
                _ => throw new Exception("ReportData: Can't find appropriate Deserializer for type " + enum_type),
            };
        }
    }
}
