﻿using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CombatLogParser
{
    public class MatchRecorder
    {
        public delegate void OnReportFindDelegate(ReportKey key, ReportData data);
        private Delegate OnReportFind;
        private Parser Parser;
        public List<BaseLogEntry> Events { get; private set; }
        private int Zone = 0;
        private string ZoneName = "";
        private string ArenaFormat = "";

        public MatchRecorder(OnReportFindDelegate onReportFind)
        {
            Events = new List<BaseLogEntry>();
            Parser = new Parser();
            OnReportFind = onReportFind;

            Clear();
        }

        public MatchRecorder(OnReportFindDelegate onReportFind, TimeZoneInfo timeZone)
        {
            Events = new List<BaseLogEntry>();
            Parser = new Parser(timeZone);
            OnReportFind = onReportFind;

            Clear();
        }

        public void Clear()
        {
            Parser.Prepare();
            Events.Clear();
            Zone = 0;
            ZoneName = "";
            ArenaFormat = "3v3";
        }

        public void ChangeZone(ZoneChange zch)
        {
            System.Diagnostics.Debug.WriteLine(this.GetType().Name, "Zone Change: " + zch);

            if (Enum.GetValues(typeof(ZoneID)).Cast<int>().Any(v => v == zch.Id))
            {
                if (Zone != 0)
                {
                    Clear();
                }
                Zone = zch.Id;
                ZoneName = zch.Name;
            }
            else
            {
                if (Zone != 0)
                {
                    EndOfArena(zch);
                }
                else
                {
                    // nothing to do, so chill.
                }
            }
        }

        public void EndOfArena(ZoneChange zch)
        {
            DateTime end_time = zch.TimeStamp;
            ArenaMatchEnd endResult = Events.FirstOrDefault(elem => elem.Type == LogEntryType.ARENA_MATCH_END) as ArenaMatchEnd;
            if (endResult != null)
            {
                end_time = endResult.TimeStamp;
            }

            List<BaseLogEntry> combatInfo = Events.FindAll(elem => elem.Type == LogEntryType.COMBATANT_INFO);

            foreach (var c in combatInfo)
            {
                try
                {
                    CombatantInfo ci = (CombatantInfo)c;
                    ci.PlayerName = Parser.Subjects.First(s => s.Value.GUID == ci.GUID).Value.Name;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(GetType().Name, "Failed to find Combatant subject");
#if DEBUG
                    throw new Exception(GetType().Name + ": Failed to find Combatant subject.\n" + ex.StackTrace);
#endif
                }
            }

            ReportKey rep = new ReportKey(end_time, ZoneName, Zone, ArenaFormat, combatInfo, endResult, Parser.Issuer);
            ReportData data = new ReportData(new List<BaseLogEntry>(Events), new Dictionary<string, LogSubject>(Parser.Subjects), new Dictionary<int, LogSpell>(Parser.Spells), Parser.GetLocale());
            System.Diagnostics.Debug.WriteLine(GetType().Name, "adding" + data.DebugReportShort() + " for report " + rep + ", Issuer is " + Parser.Issuer);

            OnReportFind.DynamicInvoke(rep, data);

            Clear();
        }

        public void EndOfArena(ArenaMatchEnd endResult)
        {
            List<BaseLogEntry> combatInfo = Events.FindAll(elem => elem.Type == LogEntryType.COMBATANT_INFO);

            foreach (var c in combatInfo)
            {
                try
                {
                    CombatantInfo ci = (CombatantInfo)c;
                    ci.PlayerName = Parser.Subjects.First(s => s.Value.GUID == ci.GUID).Value.Name;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(GetType().Name, "Failed to find Combatant subject");
#if DEBUG
                    throw new Exception(GetType().Name + ": Failed to find Combatant subject.\n" + ex.StackTrace);
#endif
                }
            }

            ReportKey rep = new ReportKey(endResult.TimeStamp, ZoneName, Zone, ArenaFormat, combatInfo, endResult, Parser.Issuer);
            ReportData data = new ReportData(new List<BaseLogEntry>(Events), new Dictionary<string, LogSubject>(Parser.Subjects), new Dictionary<int, LogSpell>(Parser.Spells), Parser.GetLocale());
            System.Diagnostics.Debug.WriteLine(GetType().Name, "adding" + data.DebugReportShort() + " for report " + rep + ", Issuer is " + Parser.Issuer);

            OnReportFind.DynamicInvoke(rep, data);

            Clear();
        }

        public void GetLine(string line)
        {
            if (line.Contains("ZONE_CHANGE"))
            {
                BaseLogEntry _entry = Parser.ParseLogEntry(line);
                if (_entry.IsValid)
                {
                    ZoneChange zch = (ZoneChange)_entry;
                    ChangeZone(zch);
                    return;
                }
            }
            else if (line.Contains("ARENA_MATCH_START"))
            {
                BaseLogEntry _entry = Parser.ParseLogEntry(line);
                ArenaMatchStart ams = (ArenaMatchStart)_entry;
                ArenaFormat = ams.Format;
                Zone = ams.ZoneId;
                Events.Clear(); // Clear flag-room events
                Events.Add(_entry);
                return;
            }
            else if (line.Contains("ARENA_MATCH_END"))
            {
                BaseLogEntry _entry = Parser.ParseLogEntry(line);
                Events.Add(_entry);
                EndOfArena((ArenaMatchEnd)_entry);
                return;
            }

            if (Zone == 0)
                return;

            BaseLogEntry entry = Parser.ParseLogEntry(line);
            if (!entry.IsValid)
            {
                System.Diagnostics.Debug.WriteLine(this.GetType().Name, "Failed to parse event properly: " + entry);
            }
            Events.Add(entry);
        }

        public void End()
        {
            if (Zone == 0)
                return;

            System.Diagnostics.Debug.WriteLine(GetType().Name, "Match in progress, but we end recording, so drop it");
            Clear();
        }

    }
}
