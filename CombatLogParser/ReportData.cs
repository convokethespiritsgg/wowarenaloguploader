﻿using CombatLogParser.Localization;
using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace CombatLogParser
{
    public class ReportData
    {
        public List<BaseLogEntry> Events { get; set; }
        public List<LogSubject> Subjects { get; set; }
        public List<LogSpell> Spells { get; set; }
        public int ZoneId { get; set; }
        public Locale Loc { get; set; }

        public ReportData()
        {
            Events = new List<BaseLogEntry>();
            Subjects = new List<LogSubject>();
            Spells = new List<LogSpell>();
        }

        public ReportData(List<BaseLogEntry> events, Dictionary<string, LogSubject> subjects, Dictionary<int, LogSpell> spells, Locale loc)
        {
            Events = events;
            Subjects = subjects.Values.ToList();
            Spells = spells.Values.ToList();
            Loc = loc;
        }

        public string GetNameByGUID(string guid)
        {
            foreach (LogSubject sub in  Subjects)
            {
                if (sub.GUID == guid)
                    return sub.Name;
            }
            return "";
        }

        public string Serialize()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"events\": [");

            sb.Append(JsonCombatLogSerializer.SerealizeCollection(Events));

            sb.Append("],");

            sb.Append("\"subjects\": [");

            sb.Append(JsonCombatLogSerializer.SerealizeCollection(Subjects));

            sb.Append("],");

            sb.Append("\"spells\": [");

            sb.Append(JsonCombatLogSerializer.SerealizeCollection(Spells));

            sb.Append("],");

            sb.Append("\"locale\": ");
            sb.Append("\"" + Loc.ToString() + "\"");

            return sb.ToString();
        }

        public static ReportData Deserialize(JsonElement json)
        {
            var events = json.GetProperty("events");

            List<BaseLogEntry> eventList = JsonCombatLogSerializer.DeserializeLogEntryCollection(events);

            var subjects = json.GetProperty("subjects");

            List<LogSubject> subjectList = JsonCombatLogSerializer.DeserializeCollection<LogSubject>(subjects);

            var spells = json.GetProperty("spells");

            List<LogSpell> spellList = JsonCombatLogSerializer.DeserializeCollection<LogSpell>(spells);

            var locale = json.GetProperty("locale");
            Locale loc = LocaleString.Parse(locale.ToString());

            ReportData data = new ReportData
            {
                Events = eventList,
                Subjects = subjectList,
                Spells = spellList,
                Loc = loc
            };

            return data;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach(BaseLogEntry entry in Events)
            {
                sb.Append(entry.ToString() + "\n");
            }

            foreach (LogSubject entry in Subjects)
            {
                sb.Append(entry.ToString() + "\n");
            }

            foreach (LogSpell entry in Spells)
            {
                sb.Append(entry.ToString() + "\n");
            }

            return sb.ToString();
        }

        public string DebugReportShort()
        {
            return String.Format("ReportData ({0} events, {1} subjects, {2} spells)", Events.Count, Subjects.Count, Spells.Count);
        }

        public List<CoordinatesByTime> GetAllPositions(string GUID)
        {
            List<CoordinatesByTime> lst = new List<CoordinatesByTime>();
            foreach (BaseLogEntry entry in Events)
            {
                switch(entry.Type)
                {
                    case LogEntryType.SPELL_DAMAGE:
                        {
                            SpellDamage sd = entry as SpellDamage;
                            lst.Add(new CoordinatesByTime(sd.TimeStamp, sd.TargetPositionX, sd.TargetPositionY, sd.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.ENVIRONMENTAL_DAMAGE:
                        {
                            EnvironmentalDamage ed = entry as EnvironmentalDamage;
                            lst.Add(new CoordinatesByTime(ed.TimeStamp, ed.TargetPositionX, ed.TargetPositionY, ed.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SWING_DAMAGE:
                        {
                            SwingDamage sd = entry as SwingDamage;
                            lst.Add(new CoordinatesByTime(sd.TimeStamp, sd.TargetPositionX, sd.TargetPositionY, sd.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SWING_DAMAGE_LANDED:
                        {
                            SwingDamageLanded sdl = entry as SwingDamageLanded;
                            lst.Add(new CoordinatesByTime(sdl.TimeStamp, sdl.TargetPositionX, sdl.TargetPositionY, sdl.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_DRAIN:
                        {
                            SpellDrain sd = entry as SpellDrain;
                            lst.Add(new CoordinatesByTime(sd.TimeStamp, sd.TargetPositionX, sd.TargetPositionY, sd.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_HEAL:
                        {
                            SpellHeal sh = entry as SpellHeal;
                            lst.Add(new CoordinatesByTime(sh.TimeStamp, sh.TargetPositionX, sh.TargetPositionY, sh.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_ENERGIZE:
                        {
                            SpellEnergize se = entry as SpellEnergize;
                            lst.Add(new CoordinatesByTime(se.TimeStamp, se.TargetPositionX, se.TargetPositionY, se.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_PERIODIC_DAMAGE:
                        {
                            SpellPeriodicDamage spd = entry as SpellPeriodicDamage;
                            lst.Add(new CoordinatesByTime(spd.TimeStamp, spd.TargetPositionX, spd.TargetPositionY, spd.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_PERIODIC_ENERGIZE:
                        {
                            SpellPeriodicEnergize spe = entry as SpellPeriodicEnergize;
                            lst.Add(new CoordinatesByTime(spe.TimeStamp, spe.TargetPositionX, spe.TargetPositionY, spe.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_PERIODIC_HEAL:
                        {
                            SpellPeriodicHeal sph = entry as SpellPeriodicHeal;
                            lst.Add(new CoordinatesByTime(sph.TimeStamp, sph.TargetPositionX, sph.TargetPositionY, sph.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.RANGE_DAMAGE:
                        {
                            RangeDamage rd = entry as RangeDamage;
                            lst.Add(new CoordinatesByTime(rd.TimeStamp, rd.TargetPositionX, rd.TargetPositionY, rd.TargetPositionZ));
                            break;
                        }
                    case LogEntryType.SPELL_CAST_SUCCESS:
                        {
                            SpellCastSuccess scs = entry as SpellCastSuccess;
                            lst.Add(new CoordinatesByTime(scs.TimeStamp, scs.TargetPositionX, scs.TargetPositionY, scs.TargetPositionZ));
                            break;
                        }
                }
            }
            return lst;
        }
    }
}
