﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class PartyKill : CombatLogEntry
    {
        public string Unknown { get; set; }

        public PartyKill() : base() { }

        public PartyKill(CombatLogEntry entry, string unknown) : base(entry)
        {
            Unknown = unknown;
        }

        public override string ToString()
        {
            return base.ToString() + ", Unknown field: " + Unknown;
        }
    }
}
