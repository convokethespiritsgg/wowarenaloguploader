﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellAbsorbed : CombatLogEntry
    {
        public string TargetHelperGUID { get; set; }
        public int AbsorbSpellId { get; set; }
        public int AbsorbedAmount { get; set; }
        public int InitialDamage { get; set; }
        public int AttackSpellId { get; set; }
        public bool IsCrit { get; set; }

        public SpellAbsorbed() : base() { }

        public SpellAbsorbed(CombatLogEntry baseEntry, string targetHelperGUID, int targetHelperSpellId, int absorbedAmount, int attackSpellId, int initialDamage, bool isCrit) : base(baseEntry)
        {
            TargetHelperGUID = targetHelperGUID;
            AbsorbSpellId = targetHelperSpellId;
            AbsorbedAmount = absorbedAmount;
            AttackSpellId = attackSpellId;
            InitialDamage = initialDamage;
            IsCrit = isCrit;
        }

        public override string ToString()
        {
            return base.ToString() + ", TargetHelperGUID: " + TargetHelperGUID + 
                                     ", AbsorbSpellId: " + AbsorbSpellId + 
                                     ", AbsorbedAmount: " + AbsorbedAmount + 
                                     ", InitialDamage: " + InitialDamage + "(Actual damage = " + (InitialDamage - AbsorbedAmount) + ")" +
                                     ", AttackSpellId: " + AttackSpellId + 
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
