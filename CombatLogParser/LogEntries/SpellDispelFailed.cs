﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellDispelFailed : SpellLogEntry
    {
        public int FailedSpellId { get; set; }

        public SpellDispelFailed() : base() { }

        public SpellDispelFailed(SpellLogEntry entry, int intSpellId) : base(entry)
        {
            FailedSpellId = intSpellId;
        }

        public override string ToString()
        {
            return base.ToString() + ", FailedSpellId: " + FailedSpellId;
        }
    }
}
