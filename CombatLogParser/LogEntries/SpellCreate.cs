﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries
{
    public class SpellCreate : SpellLogEntry
    {
        public SpellCreate() : base() { }

        public SpellCreate(SpellLogEntry baseEntry) : base(baseEntry)
        {
        }
    }
}
