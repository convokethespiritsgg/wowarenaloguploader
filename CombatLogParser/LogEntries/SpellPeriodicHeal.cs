﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellPeriodicHeal : SpellLogEntry
    {
        public int Amount { get; set; }
        public int OverHeal { get; set; }
        public int TargetHPCurrent { get; set; }
        public int TargetHPMax { get; set; }
        public int TargetResouceCurrent { get; set; }
        public int TargetResourceMax { get; set; }
        public float TargetPositionX { get; set; }
        public float TargetPositionY { get; set; }
        public float TargetPositionZ { get; set; }
        public int TargetItemLevel { get; set; }
        public bool IsCrit { get; set; }

        public SpellPeriodicHeal() : base() { }

        public SpellPeriodicHeal(SpellLogEntry entry, int hpCurrent, int hpMax, int resourceCurrent, int resourceMax, float posX, float posY, float posZ, int itemLvl, float amount, float overAmount, bool isCrit) : base(entry)
        {
            TargetHPMax = hpMax;
            TargetHPCurrent = hpCurrent;
            Amount = (int)amount;
            OverHeal = (int)overAmount;
            TargetResouceCurrent = resourceCurrent;
            TargetResourceMax = resourceMax;
            TargetPositionX = posX;
            TargetPositionY = posY;
            TargetPositionZ = posZ;
            TargetItemLevel = itemLvl;
            IsCrit = isCrit;
        }

        public override string ToString()
        {
            return base.ToString() + ", TargetHP: " + TargetHPCurrent + "/" + TargetHPMax +
                                     ", TargetResouce: " + TargetResouceCurrent + "/" + TargetResourceMax +
                                     ", TargetPosition: " + TargetPositionX + " " + TargetPositionY + " " + TargetPositionZ +
                                     ", ilvl: " + TargetItemLevel +  
                                     ", Amount: " + Amount +
                                     ", OverHeal: " + OverHeal +
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
