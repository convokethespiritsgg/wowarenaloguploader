﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.Covenant
{
    public class CombatantCovenantInfo
    {
        public CovenantChosen Covenant { get; set; }
        public SoulbindChosen Soulbind { get; set; }
        public List<int> SoulbindTree { get; set; }
        public List<KeyValuePair<int, int>> Conduits { get; set; }
        public CombatantCovenantInfo()
        {
            SoulbindTree = new List<int>();
            Conduits = new List<KeyValuePair<int, int>>();
        }

        public override string ToString()
        {
            return "CovenantInfo: " + Covenant + " " + Soulbind +
                " " + String.Join(" ", SoulbindTree) + " " + String.Join(" ", Conduits);
        }
    }
}
