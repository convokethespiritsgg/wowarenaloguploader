﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.Covenant
{
    public enum CovenantChosen
    {
        Kyrian = 1,
        Venthyr = 2,
        NightFae = 3,
        Necrolord = 4,
        Unknown = 0
    }

    public static class CovenantParser
    {
        public static CovenantChosen Parse(int covId)
        {
            return covId switch
            {
                1 => CovenantChosen.Kyrian,
                2 => CovenantChosen.Venthyr,
                3 => CovenantChosen.NightFae,
                4 => CovenantChosen.Necrolord,
                _ => CovenantChosen.Unknown
            };
        }
    }
}
