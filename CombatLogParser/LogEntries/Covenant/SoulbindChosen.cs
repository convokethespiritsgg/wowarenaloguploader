﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.Covenant
{
    public enum SoulbindChosen
    {
        Niya = 1,
        DreamWeaver = 2,
        GeneralDraven = 3,
        PlagueDeviserMarileth = 4,
        Emeni = 5,
        Korayn = 6,
        Pelagos = 7,
        NadjiaTheMistBlade = 8,
        TheotarTheMadDuke = 9,
        BonesmithHeimir = 10,
        Kleia = 13,
        ForgelitePrimeMikanikos = 18,
        Unknown = 0
    }

    public static class SoubindParser
    {
        public static SoulbindChosen Parse(int sbId)
        {
            return sbId switch
            {
                1 => SoulbindChosen.Niya,
                2 => SoulbindChosen.DreamWeaver,
                3 => SoulbindChosen.GeneralDraven,
                4 => SoulbindChosen.PlagueDeviserMarileth,
                5 => SoulbindChosen.Emeni,
                6 => SoulbindChosen.Korayn,
                7 => SoulbindChosen.Pelagos,
                8 => SoulbindChosen.NadjiaTheMistBlade,
                9 => SoulbindChosen.TheotarTheMadDuke,
                10 => SoulbindChosen.BonesmithHeimir,
                13 => SoulbindChosen.Kleia,
                18 => SoulbindChosen.ForgelitePrimeMikanikos,
                _ => SoulbindChosen.Unknown
            };
        }
    }
}
