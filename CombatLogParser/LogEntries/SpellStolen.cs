﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellStolen : SpellLogEntry
    {
        public int StolenSpellId { get; set; }
        public bool IsBuff { get; set; }

        public SpellStolen() : base() { }

        public SpellStolen(SpellLogEntry entry, int spellId, bool isBuff) : base(entry)
        {
            StolenSpellId = spellId;
            IsBuff = isBuff;
        }

        public override string ToString()
        {
            return base.ToString() + ", StolenSpellId: " + StolenSpellId + (IsBuff ? ", Buff" : ", Debuff");
        }

    }
}
