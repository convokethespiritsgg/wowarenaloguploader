﻿using System;
using System.Collections.Generic;
using System.Text;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class CombatLogVersion: EnvironmentLogEntry
    {
        public int Version { get; set; }
        public bool AdvancedLogEnabled { get; set; }
        public string WowBuildVersion { get; set; }
        public int ProjectID { get; set; }

        public CombatLogVersion(): base() { }

        public CombatLogVersion(DateTime timeStamp, int version, bool adv_log, string build_version, int project_id): base(timeStamp, LogEntryType.COMBAT_LOG_VERSION)
        {
            Version = version;
            AdvancedLogEnabled = adv_log;
            WowBuildVersion = build_version;
            ProjectID = project_id;
        }

        public override string ToString()
        {
            return base.ToString() + ", Version: " + Version +
                                     ", AdvancedLogEnabled: " + AdvancedLogEnabled +
                                     ", WowBuildVersion: " + WowBuildVersion +
                                     ", ProjectID: " + ProjectID;
        }
    }
}
