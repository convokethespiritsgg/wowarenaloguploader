﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellMissed : SpellLogEntry
    {
        public LogEntryMissType MissType { get; set; }
        public bool IsOffHand { get; set; }
        public int Absorbed { get; set; }
        public int InitialDamage { get; set; }
        public bool IsCrit { get; set; }

        public SpellMissed() : base() { }

        public SpellMissed(SpellLogEntry entry, LogEntryMissType missType, bool isOffHand, int amountAbsorbed, int initialDamage, bool isCrit) : base(entry)
        {
            MissType = missType;
            IsOffHand = isOffHand;
            Absorbed = amountAbsorbed;
            InitialDamage = initialDamage;
            IsCrit = isCrit;
        }

        public override string ToString()
        {
            return base.ToString() + ", MissType: " + MissType.ToString() + " (val: " + MissType + ")" + 
                                     ", IsOffHand: " + IsOffHand + 
                                     ", Absorbed: " + Absorbed + 
                                     ", InitialDamage: " + InitialDamage + 
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
