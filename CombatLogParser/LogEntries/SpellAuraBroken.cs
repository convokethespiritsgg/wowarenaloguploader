﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellAuraBroken : SpellLogEntry
    {
        public bool IsBuff { get; set; }

        public SpellAuraBroken() : base() { }

        public SpellAuraBroken(SpellLogEntry entry, bool isBuff) : base(entry)
        {
            IsBuff = isBuff;
        }

        public override string ToString()
        {
            return base.ToString() + (IsBuff ? ", Buff" : ", Debuff");
        }
    }
}
