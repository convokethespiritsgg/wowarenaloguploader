﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellCastSuccess : SpellLogEntry
    {
        public string SpellTargetGUID { get; set; }
        public float TargetPositionX { get; set; }
        public float TargetPositionY { get; set; }
        public float TargetPositionZ { get; set; }
        public int TargetItemLevel { get; set; }

        public SpellCastSuccess() : base() { }

        public SpellCastSuccess(SpellLogEntry entry, string spellTargetGuid, float posX, float posY, float posZ, int ilvl): base(entry)
        {
            SpellTargetGUID = spellTargetGuid;
            TargetPositionX = posX;
            TargetPositionY = posY;
            TargetPositionZ = posZ;
            TargetItemLevel = ilvl;
        }

        public override string ToString()
        {
            return base.ToString() + ", SpellTargetGUID: " + SpellTargetGUID +
                                     ", TargetPosition: " + TargetPositionX + " " + TargetPositionY + " " + TargetPositionZ +
                                     ", ilvl: " + TargetItemLevel;
        }
    }
}
