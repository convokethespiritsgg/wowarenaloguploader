﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;
using CombatLogParser.LogEntries.Covenant;

namespace CombatLogParser.LogEntries
{
    public class CombatantInfo : EnvironmentLogEntry
    {
        // TODO: parse main and secondary stats!
        public int RawSpecId { get; set; }
        public ClassSpec Spec { get; set; }
        public string GUID { get; set; }
        public int TeamNumber { get; set; }
        public int TalentRow15 { get; set; }
        public int TalentRow25 { get; set; }
        public int TalentRow30 { get; set; }
        public int TalentRow35 { get; set; }
        public int TalentRow40 { get; set; }
        public int TalentRow45 { get; set; }
        public int TalentRow50 { get; set; }
        public int TalentPvP1 { get; set; }
        public int TalentPvP2 { get; set; }
        public int TalentPvP3 { get; set; }
        public CombatantCovenantInfo CovenantInfo { get; set; }
        public List<EquipmentItem> Items { get; set; }
        public string PlayerName { get; set; }

        public CombatantInfo()
        {
            Items = new List<EquipmentItem>();
            PlayerName = "";
        }

        public CombatantInfo(DateTime time, int rawSpecId, ClassSpec unitspec,
            string guid, int tal15, int tal25, int tal30, int tal35, int tal40, int tal45, int tal50,
            int pvptal1, int pvptal2, int pvptal3, CombatantCovenantInfo covInfo, List<EquipmentItem> items,
            int team_number): base(time, LogEntryType.COMBATANT_INFO)
        {
            Spec = unitspec;
            GUID = guid;
            RawSpecId = rawSpecId;
            TalentRow15 = tal15;
            TalentRow25 = tal25;
            TalentRow30 = tal30;
            TalentRow35 = tal35;
            TalentRow40 = tal40;
            TalentRow45 = tal45;
            TalentRow50 = tal50;
            TalentPvP1 = pvptal1;
            TalentPvP2 = pvptal2;
            TalentPvP3 = pvptal3;
            CovenantInfo = covInfo;
            Items = items;
            TeamNumber = team_number;
            PlayerName = "";
        }

        public override string ToString()
        {
            return base.ToString() + ", Class: " + PlayerClassConv.ClassToString(Spec) + ", Spec: " + PlayerClassConv.SpecToString(Spec) + ", GUID: " + GUID +
                ", " + CovenantInfo.ToString() + ", " + "Items: " + String.Join(", ", Items);
        }
    }
}
