﻿using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser
{
    public class UnitDestroyed : CombatLogEntry
    {
        public int UnknownField { get; set; }

        public UnitDestroyed() : base() { }

        public UnitDestroyed(CombatLogEntry entry, int field): base(entry)
        {
            UnknownField = field;
        }

        public override string ToString()
        {
            return base.ToString() + ", UnknownField: " + UnknownField;
        }
    }
}