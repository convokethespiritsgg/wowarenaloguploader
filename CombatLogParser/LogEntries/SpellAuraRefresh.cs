﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellAuraRefresh : SpellLogEntry
    {
        public bool IsBuff { get; set; }

        public SpellAuraRefresh() : base() { }

        public SpellAuraRefresh(SpellLogEntry entry, bool isBuff) : base(entry)
        {
            IsBuff = isBuff;
        }

        public override string ToString()
        {
            return base.ToString() + (IsBuff ? ", Buff" : ", Debuff");
        }

    }
}
