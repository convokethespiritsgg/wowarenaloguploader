﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.Localization;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellCastFailed : SpellLogEntry
    {
        public GlobalString FailReason { get; set; }
        public string DetailedReason { get; set; }

        public SpellCastFailed() : base() { }

        public SpellCastFailed(SpellLogEntry entry, GlobalString reason) : base(entry)
        {
            FailReason = reason;
        }

        public SpellCastFailed(SpellLogEntry entry, GlobalString reason, string detail) : base(entry)
        {
            FailReason = reason;
            DetailedReason = detail;
        }

        public override string ToString()
        {
            return base.ToString() + ", FailReason: " + FailReason;
        }
    }
}
