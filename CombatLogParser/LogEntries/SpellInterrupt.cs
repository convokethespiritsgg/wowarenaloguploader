﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellInterrupt : SpellLogEntry
    {
        public int InterruptedSpellId { get; set; }

        public SpellInterrupt() : base() { }

        public SpellInterrupt(SpellLogEntry entry, int intSpellId) : base(entry)
        {
            InterruptedSpellId = intSpellId;
        }

        public override string ToString()
        {
            return base.ToString() + ", InterruptedSpellId: " + InterruptedSpellId;
        }
    }
}
