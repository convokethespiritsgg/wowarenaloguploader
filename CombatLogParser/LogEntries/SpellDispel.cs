﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellDispel : SpellLogEntry
    {
        public int DispelledSpellId { get; set; }
        public bool IsBuff { get; set; }

        public SpellDispel() : base() { }

        public SpellDispel(SpellLogEntry entry, int spellId, bool isBuff) : base(entry)
        {
            DispelledSpellId = spellId;
            IsBuff = isBuff;
        }

        public override string ToString()
        {
            return base.ToString() + ", DispelledSpellId: " + DispelledSpellId + (IsBuff ? ", Buff" : ", Debuff");
        }
    }
}
