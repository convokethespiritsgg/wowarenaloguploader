﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellAuraRemovedDose : SpellLogEntry
    {
        public bool IsBuff { get; set; }
        public int Amount { get; set; }

        public SpellAuraRemovedDose() : base() { }
        public SpellAuraRemovedDose(SpellLogEntry entry, bool isBuff, int amount) : base(entry)
        {
            IsBuff = isBuff;
            Amount = amount;
        }

        public override string ToString()
        {
            return base.ToString() + (IsBuff ? ", Buff" : ", Debuff");
        }

    }
}
