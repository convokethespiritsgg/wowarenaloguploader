﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class UnitDied : CombatLogEntry
    {
        public string Unknown { get; set; }

        public UnitDied() { }

        public UnitDied(CombatLogEntry entry, string unknown): base(entry)
        {
            Unknown = unknown;
        }

        public override string ToString()
        {
            return base.ToString() + ", Unknown field: " + Unknown;
        }
    }
}
