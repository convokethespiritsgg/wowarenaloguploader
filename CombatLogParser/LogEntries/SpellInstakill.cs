﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries
{
    public class SpellInstakill: SpellLogEntry
    {
        public int UnknownField { get; set; }

        public SpellInstakill() : base() { }

        public SpellInstakill(SpellLogEntry entry, int field): base(entry)
        {
            UnknownField = field;
        }

        public override string ToString()
        {
            return base.ToString() + ", UnknownField: " + UnknownField;
        }
    }
}
