﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class LogSpellConst
    {
        public const int INVALID_SPELL_ID = -1;
        public const int WHITE_MELEE_ATTACK = 6603;
        public const int WHITE_RANGE_ATTACK = 75;
    }

    public class LogSpell: IComparable<LogSpell>, IEquatable<LogSpell>
    {
        public int SpellId { get; set; }
        public LogEntrySchoolType SpellSchool { get; set; }

        public LogSpell() { }

        public LogSpell(int spellId, LogEntrySchoolType spellSchool)
        {
            SpellId = spellId;
            SpellSchool = spellSchool;
        }

        public static LogSpell ParseSpell(string[] parameters)
        {
            int id = int.Parse(parameters[0]);
            LogEntrySchoolType type = LogEntryTypeConv.SchoolFromString(parameters[2]);
            return new LogSpell(id, type);
        }

        public int CompareTo(LogSpell other)
        {
            return (other == null) ? -1 : (this.SpellId - other.SpellId);
        }

        public bool Equals(LogSpell other)
        {
            return other != null && (this.SpellId == other.SpellId);
        }

        public override string ToString()
        {
            return "LogSpell => Spell ID: " + this.SpellId  + ", School: " + this.SpellSchool;
        }
    }
}
