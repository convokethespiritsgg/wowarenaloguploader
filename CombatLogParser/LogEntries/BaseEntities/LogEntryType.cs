﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public enum LogEntryType
    {
        UNKNOWN,
        ARENA_MATCH_END,
        ARENA_MATCH_START,
        COMBAT_LOG_VERSION,
        COMBATANT_INFO,
        DAMAGE_SPLIT,
        ENVIRONMENTAL_DAMAGE,
        PARTY_KILL,
        RANGE_DAMAGE,
        RANGE_MISSED,
        SPELL_ABSORBED,
        SPELL_AURA_APPLIED,
        SPELL_AURA_APPLIED_DOSE,
        SPELL_AURA_BROKEN,
        SPELL_AURA_BROKEN_SPELL,
        SPELL_AURA_REFRESH,
        SPELL_AURA_REMOVED,
        SPELL_AURA_REMOVED_DOSE,
        SPELL_CAST_FAILED,
        SPELL_CAST_START,
        SPELL_CAST_SUCCESS,
        SPELL_CREATE,
        SPELL_DAMAGE,
        SPELL_DISPEL,
        SPELL_DISPEL_FAILED,
        SPELL_DRAIN,
        SPELL_ENERGIZE,
        SPELL_EXTRA_ATTACKS,
        SPELL_HEAL,
        SPELL_HEAL_ABSORBED,
        SPELL_INSTAKILL,
        SPELL_INTERRUPT,
        SPELL_MISSED,
        SPELL_PERIODIC_DAMAGE,
        SPELL_PERIODIC_ENERGIZE,
        SPELL_PERIODIC_HEAL,
        SPELL_PERIODIC_MISSED,
        SPELL_STOLEN,
        SPELL_SUMMON,
        SWING_DAMAGE,
        SWING_DAMAGE_LANDED,
        SWING_MISSED,
        UNIT_DESTROYED,
        UNIT_DIED,
        ZONE_CHANGE,
    }

    public enum LogEntryMissType
    {
        MISS_ABSORB,
        MISS_BLOCK,
        MISS_DEFLECT,
        MISS_DODGE,
        MISS_EVADE,
        MISS_IMMUNE,
        MISS_PARRY,
        MISS_REFLECT,
        MISS_RESIST,
        UNKNOWN
    }

    public enum LogEntrySchoolType
    {
        UNKNOWN = 0,
        PHYSICAL     = 0x01,
        HOLY         = 0x02,
        HOLYSTRIKE   = (PHYSICAL | HOLY),
        FIRE         = 0x04,
        FLAMESTRIKE  = (FIRE | PHYSICAL),
        HOLYFIRE     = (FIRE | HOLY),
        NATURE       = 0x08,
        STORMSTRIKE  = (NATURE | PHYSICAL),
        HOLYSTORM    = (NATURE | HOLY),
        FIRESTORM    = (NATURE | FIRE),
        FROST        = 0x10,
        FROSTSTRIKE  = (FROST | PHYSICAL),
        HOLYFROST    = (FROST | HOLY),
        FROSTFIRE    = (FROST | FIRE),
        FROSTSTORM   = (FROST | NATURE),
        SHADOW       = 0x20,
        SHADOWSTRIKE = (SHADOW | PHYSICAL),
        SHADOWLIGHT  = (SHADOW | HOLY),
        SHADOWFLAME  = (SHADOW | FIRE),
        SHADOWSTORM  = (SHADOW | NATURE),
        SHADOWFROST  = (SHADOW | FROST),
        ARCANE       = 0x40,
        SPELLSTRIKE  = (ARCANE | PHYSICAL),
        DIVINE       = (ARCANE | HOLY),
        SHELLFIRE    = (ARCANE | FIRE),
        SPELLSTORM   = (ARCANE | NATURE), 
        SPELLFROST   = (ARCANE | FROST),
        SPELLSHADOW  = (ARCANE | SHADOW),
        ELEMENTAL    = (FROST | NATURE | FIRE),
        CHROMATIC    = (ARCANE | SHADOW | FROST | NATURE | FIRE),
        MAGIC        = (ARCANE | SHADOW | FROST | NATURE | FIRE | HOLY),
        CHAOS        = (ARCANE | SHADOW | FROST | NATURE | FIRE | HOLY | PHYSICAL)
    }

    public enum LogEntryPowerType
    {
        POWER_HEALTHCOST = -2,
        POWER_NONE = -1,
        POWER_MANA = 0,
        POWER_RAGE,
        POWER_FOCUS,
        POWER_ENERGY,
        POWER_COMBOPOINTS,
        POWER_RUNES,
        POWER_RUNICPOWER,
        POWER_SOULSHARDS,
        POWER_LUNARPOWER,
        POWER_HOLYPOWER,
        POWER_ALTERNATE,
        POWER_MAELSTROM,
        POWER_CHI,
        POWER_INSANITY,
        POWER_OBSOLETE,
        POWER_OBSOLETE2,
        POWER_ARCANECHARGES,
        POWER_FURY,
        POWER_PAIN,
        POWER_NUMPOWERTYPES,
        UNKNOWN
    }

    public sealed class LogEntryTypeConv
    {
        public static LogEntryType FromString(string str)
        {
            return str switch
            {
                "ARENA_MATCH_END"         => LogEntryType.ARENA_MATCH_END,
                "ARENA_MATCH_START"       => LogEntryType.ARENA_MATCH_START,
                "COMBAT_LOG_VERSION"      => LogEntryType.COMBAT_LOG_VERSION,
                "COMBATANT_INFO"          => LogEntryType.COMBATANT_INFO,
                "DAMAGE_SPLIT"            => LogEntryType.DAMAGE_SPLIT,
                "ENVIRONMENTAL_DAMAGE"    => LogEntryType.ENVIRONMENTAL_DAMAGE,
                "PARTY_KILL"              => LogEntryType.PARTY_KILL,
                "RANGE_DAMAGE"            => LogEntryType.RANGE_DAMAGE,
                "RANGE_MISSED"            => LogEntryType.RANGE_MISSED,
                "SPELL_ABSORBED"          => LogEntryType.SPELL_ABSORBED,
                "SPELL_AURA_APPLIED"      => LogEntryType.SPELL_AURA_APPLIED,
                "SPELL_AURA_APPLIED_DOSE" => LogEntryType.SPELL_AURA_APPLIED_DOSE,
                "SPELL_AURA_BROKEN"       => LogEntryType.SPELL_AURA_BROKEN,
                "SPELL_AURA_BROKEN_SPELL" => LogEntryType.SPELL_AURA_BROKEN_SPELL,
                "SPELL_AURA_REFRESH"      => LogEntryType.SPELL_AURA_REFRESH,
                "SPELL_AURA_REMOVED"      => LogEntryType.SPELL_AURA_REMOVED,
                "SPELL_AURA_REMOVED_DOSE" => LogEntryType.SPELL_AURA_REMOVED_DOSE,
                "SPELL_CAST_FAILED"       => LogEntryType.SPELL_CAST_FAILED,
                "SPELL_CAST_START"        => LogEntryType.SPELL_CAST_START,
                "SPELL_CAST_SUCCESS"      => LogEntryType.SPELL_CAST_SUCCESS,
                "SPELL_CREATE"            => LogEntryType.SPELL_CREATE,
                "SPELL_DAMAGE"            => LogEntryType.SPELL_DAMAGE,
                "SPELL_DISPEL"            => LogEntryType.SPELL_DISPEL,
                "SPELL_DRAIN"             => LogEntryType.SPELL_DRAIN,
                "SPELL_ENERGIZE"          => LogEntryType.SPELL_ENERGIZE,
                "SPELL_EXTRA_ATTACKS"     => LogEntryType.SPELL_EXTRA_ATTACKS,
                "SPELL_HEAL"              => LogEntryType.SPELL_HEAL,
                "SPELL_HEAL_ABSORBED"     => LogEntryType.SPELL_HEAL_ABSORBED,
                "SPELL_INSTAKILL"         => LogEntryType.SPELL_INSTAKILL,
                "SPELL_INTERRUPT"         => LogEntryType.SPELL_INTERRUPT,
                "SPELL_MISSED"            => LogEntryType.SPELL_MISSED,
                "SPELL_PERIODIC_DAMAGE"   => LogEntryType.SPELL_PERIODIC_DAMAGE,
                "SPELL_PERIODIC_ENERGIZE" => LogEntryType.SPELL_PERIODIC_ENERGIZE,
                "SPELL_PERIODIC_HEAL"     => LogEntryType.SPELL_PERIODIC_HEAL,
                "SPELL_PERIODIC_MISSED"   => LogEntryType.SPELL_PERIODIC_MISSED,
                "SPELL_STOLEN"            => LogEntryType.SPELL_STOLEN,
                "SPELL_SUMMON"            => LogEntryType.SPELL_SUMMON,
                "SWING_DAMAGE"            => LogEntryType.SWING_DAMAGE,
                "SWING_DAMAGE_LANDED"     => LogEntryType.SWING_DAMAGE_LANDED,
                "SWING_MISSED"            => LogEntryType.SWING_MISSED,
                "UNIT_DESTROYED"          => LogEntryType.UNIT_DESTROYED,
                "UNIT_DIED"               => LogEntryType.UNIT_DIED,
                "ZONE_CHANGE"             => LogEntryType.ZONE_CHANGE,
                _                         => LogEntryType.UNKNOWN,
            };
        }

        public static LogEntryMissType MissFromString(string str)
        {
            return str switch
            {
                "ABSORB"  => LogEntryMissType.MISS_ABSORB,
                "BLOCK"   => LogEntryMissType.MISS_BLOCK,
                "DEFLECT" => LogEntryMissType.MISS_DEFLECT,
                "DODGE"   => LogEntryMissType.MISS_DODGE,
                "EVADE"   => LogEntryMissType.MISS_EVADE,
                "IMMUNE"  => LogEntryMissType.MISS_IMMUNE,
                "PARRY"   => LogEntryMissType.MISS_PARRY,
                "REFLECT" => LogEntryMissType.MISS_REFLECT,
                "RESIST"  => LogEntryMissType.MISS_RESIST,
                _         => LogEntryMissType.UNKNOWN,
            };
        }


        public static bool LogEntryIsCombat(LogEntryType type)
        {
            switch (type)
            {
                case LogEntryType.ARENA_MATCH_END:
                case LogEntryType.ARENA_MATCH_START:
                case LogEntryType.COMBATANT_INFO:
                case LogEntryType.COMBAT_LOG_VERSION:
                case LogEntryType.ZONE_CHANGE:
                    return false;
                case LogEntryType.DAMAGE_SPLIT:
                case LogEntryType.ENVIRONMENTAL_DAMAGE:
                case LogEntryType.PARTY_KILL:
                case LogEntryType.RANGE_DAMAGE:
                case LogEntryType.RANGE_MISSED:
                case LogEntryType.SPELL_ABSORBED:
                case LogEntryType.SPELL_AURA_APPLIED:
                case LogEntryType.SPELL_AURA_APPLIED_DOSE:
                case LogEntryType.SPELL_AURA_BROKEN:
                case LogEntryType.SPELL_AURA_BROKEN_SPELL:
                case LogEntryType.SPELL_AURA_REFRESH:
                case LogEntryType.SPELL_AURA_REMOVED:
                case LogEntryType.SPELL_AURA_REMOVED_DOSE:
                case LogEntryType.SPELL_CAST_FAILED:
                case LogEntryType.SPELL_CAST_START:
                case LogEntryType.SPELL_CAST_SUCCESS:
                case LogEntryType.SPELL_CREATE:
                case LogEntryType.SPELL_DAMAGE:
                case LogEntryType.SPELL_DISPEL:
                case LogEntryType.SPELL_DISPEL_FAILED:
                case LogEntryType.SPELL_DRAIN:
                case LogEntryType.SPELL_ENERGIZE:
                case LogEntryType.SPELL_EXTRA_ATTACKS:
                case LogEntryType.SPELL_HEAL:
                case LogEntryType.SPELL_HEAL_ABSORBED:
                case LogEntryType.SPELL_INSTAKILL:
                case LogEntryType.SPELL_INTERRUPT:
                case LogEntryType.SPELL_MISSED:
                case LogEntryType.SPELL_PERIODIC_DAMAGE:
                case LogEntryType.SPELL_PERIODIC_ENERGIZE:
                case LogEntryType.SPELL_PERIODIC_HEAL:
                case LogEntryType.SPELL_PERIODIC_MISSED:
                case LogEntryType.SPELL_STOLEN:
                case LogEntryType.SPELL_SUMMON:
                case LogEntryType.SWING_DAMAGE:
                case LogEntryType.SWING_DAMAGE_LANDED:
                case LogEntryType.SWING_MISSED:
                case LogEntryType.UNIT_DESTROYED:
                case LogEntryType.UNIT_DIED:
                    return true;
                default:
                    break;
            }
            return false;
        }

        public static bool LogEntryIsSpell(LogEntryType type)
        {
            switch (type)
            {
                case LogEntryType.DAMAGE_SPLIT:
                case LogEntryType.RANGE_DAMAGE:
                case LogEntryType.RANGE_MISSED:
                case LogEntryType.SPELL_ABSORBED:
                case LogEntryType.SPELL_AURA_APPLIED:
                case LogEntryType.SPELL_AURA_APPLIED_DOSE:
                case LogEntryType.SPELL_AURA_BROKEN:
                case LogEntryType.SPELL_AURA_BROKEN_SPELL:
                case LogEntryType.SPELL_AURA_REFRESH:
                case LogEntryType.SPELL_AURA_REMOVED:
                case LogEntryType.SPELL_AURA_REMOVED_DOSE:
                case LogEntryType.SPELL_CAST_FAILED:
                case LogEntryType.SPELL_CAST_START:
                case LogEntryType.SPELL_CAST_SUCCESS:
                case LogEntryType.SPELL_CREATE:
                case LogEntryType.SPELL_DAMAGE:
                case LogEntryType.SPELL_DISPEL:
                case LogEntryType.SPELL_DISPEL_FAILED:
                case LogEntryType.SPELL_DRAIN:
                case LogEntryType.SPELL_ENERGIZE:
                case LogEntryType.SPELL_EXTRA_ATTACKS:
                case LogEntryType.SPELL_HEAL:
                case LogEntryType.SPELL_HEAL_ABSORBED:
                case LogEntryType.SPELL_INTERRUPT:
                case LogEntryType.SPELL_INSTAKILL:
                case LogEntryType.SPELL_MISSED:
                case LogEntryType.SPELL_PERIODIC_DAMAGE:
                case LogEntryType.SPELL_PERIODIC_ENERGIZE:
                case LogEntryType.SPELL_PERIODIC_HEAL:
                case LogEntryType.SPELL_PERIODIC_MISSED:
                case LogEntryType.SPELL_STOLEN:
                case LogEntryType.SPELL_SUMMON:
                    return true;
                default:
                    return false;
            }
        }

        public static LogEntrySchoolType SchoolFromString(string schoolId)
        {
            try
            {
                int schoolVal;
                if (!schoolId.Contains("0x"))
                    schoolVal = int.Parse(schoolId);
                else
                    schoolVal = Convert.ToInt32(schoolId, 16);
                return (LogEntrySchoolType)Enum.ToObject(typeof(LogEntrySchoolType), schoolVal);
            }
            catch (Exception)
            {
                return LogEntrySchoolType.UNKNOWN;
            }
        }

        public static LogEntryPowerType PowerFromString(string powerType)
        {
            try
            {
                int powerVal = int.Parse(powerType);
                return (LogEntryPowerType)Enum.ToObject(typeof(LogEntryPowerType), powerVal);
            }
            catch (Exception)
            {
                return LogEntryPowerType.UNKNOWN;
            }
        }
    }
}
