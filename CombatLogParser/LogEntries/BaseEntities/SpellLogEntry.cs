﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class SpellLogEntry : CombatLogEntry
    {
        public int SpellId { get; set; }

        public SpellLogEntry(): base() { }

        public SpellLogEntry(SpellLogEntry entry) : base(entry)
        {
            SpellId = entry.SpellId;
        }

        public SpellLogEntry(CombatLogEntry combatEntry, int spellId): base(combatEntry)
        {
            SpellId = spellId;
        }

        public override string ToString()
        {
            return base.ToString() + ", Spell ID: " + this.SpellId;
        }
    }
}
