﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class EquipmentItem
    {
        public int ItemId { get; set; }
        public int ItemLevel { get; set; }
        public List<int> Enchants { get; set; }
        public List<int> Bonuses { get; set; }
        public int Gem { get; set; }
        public EquipmentItem()
        {
            Enchants = new List<int>();
            Bonuses = new List<int>();
        }
        public EquipmentItem(int id, int ilvl, List<int> ench, List<int> bonus, int gem)
        {
            ItemId = id;
            ItemLevel = ilvl;
            Enchants = ench;
            Bonuses = bonus;
            Gem = gem;
        }

        public override string ToString()
        {
            return "EquipmentItem(" + ItemId + ", " + ItemLevel + ", (" + String.Join(",", Enchants) + "), (" + String.Join(",", Bonuses) + "), " + Gem + ")";
        }
    }
}
