﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class CoordinatesByTime
    {
        public DateTime TimeStamp { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public CoordinatesByTime(DateTime tm, float x, float y, float z)
        {
            TimeStamp = tm;
            X = x;
            Y = y;
            Z = z;
        }
    }
}
