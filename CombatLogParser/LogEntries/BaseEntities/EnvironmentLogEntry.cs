﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class EnvironmentLogEntry : BaseLogEntry
    {
        public EnvironmentLogEntry(): base() { }

        public EnvironmentLogEntry(DateTime time, LogEntryType type) : base(time, type)
        {
        }
    }
}
