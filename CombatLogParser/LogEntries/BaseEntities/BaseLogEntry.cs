﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class BaseLogEntry
    {
        public LogEntryType Type { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Raw { get; set; }
        public string Reason { get; set; }
        public bool IsValid { get; set; }

        public BaseLogEntry() {
            this.Type = LogEntryType.UNKNOWN;
        }

        public BaseLogEntry(DateTime time, LogEntryType type)
        {
            this.TimeStamp = time;
            this.Type = type;
            this.IsValid = true;
        }

        public BaseLogEntry(BaseLogEntry other)
        {
            this.TimeStamp = other.TimeStamp;
            this.Type = other.Type;
            this.IsValid = other.IsValid;
            //------------------------------
            this.Raw = other.Raw;
            this.Reason = other.Reason;
        }

        private BaseLogEntry(string parameters, string reason)
        {
            this.IsValid = false;
            this.Raw = parameters;
            this.Reason = reason;
            this.Type = LogEntryType.UNKNOWN;
        }

        public static BaseLogEntry InvalidEntry(string parameters, string reason)
        {
            return new BaseLogEntry(parameters, reason);
        }

        public override string ToString()
        {
            return "LogEvent => Valid:" + (IsValid ? 
                ("1, Date: " + this.TimeStamp.ToString() + ", Type: " + this.Type) :
                ("0, Reason: " + this.Reason + ", event: " + this.Raw)) ;
        }

    }
}
