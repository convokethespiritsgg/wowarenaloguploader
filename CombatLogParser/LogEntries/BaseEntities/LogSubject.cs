﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class LogSubject: IComparable<LogSubject>, IEquatable<LogSubject>
    {
        public string Name { get; set; }
        public string GUID { get; set; }
        public string Flags { get; set; }
        public string RaidFlags { get; set; }

        public LogSubject() { }

        public LogSubject(string guid, string name, string flags, string raidFlags)
        {
            Name = name;
            GUID = guid;
            Flags = flags;
            RaidFlags = raidFlags;
        }

        public static LogSubject InvalidEntry()
        {
            return new LogSubject("", "", "", "");
        }

        public static LogSubject ComparisonObject(string guid)
        {
            return new LogSubject(guid, "", "", "");
        }

        public int CompareTo(LogSubject obj)
        {
            LogSubject subject = obj as LogSubject;

            return string.Compare(this.GUID, subject.GUID);
        }

        public bool Equals(LogSubject other)
        {
            return other != null && this.CompareTo(other) == 0;
        }

        public override string ToString()
        {
            return "LogSubject => Name: " + Name + ", GUID: " + GUID + ", Flags: " + Flags + ", Raid flags: " + RaidFlags;
        }

        public static LogSubject ParseSubject(string[] parameters)
        {
            return new LogSubject(parameters[0],
                                  parameters[1].Replace("\"", ""),
                                  parameters[2].Replace("\"", ""),
                                  parameters[3]);
        }
    }
}
