﻿using System;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public enum ClassSpec
    {
        DEMON_HUNTER_HAVOC = 0,
        DEMON_HUNTER_VENGEANCE,

        DEATH_KNIGHT_UNHOLY,
        DEATH_KNIGHT_FROST,
        DEATH_KNIGHT_BLOOD,

        DRUID_GUARDIAN,
        DRUID_RESTORATION,
        DRUID_BALANCE,
        DRUID_FERAL,

        HUNTER_BEASTMASTERY,
        HUNTER_MARKSMANSHIP,
        HUNTER_SURVIVAL,

        MAGE_ARCANE,
        MAGE_FROST,
        MAGE_FIRE,

        MONK_BREWMASTER,
        MONK_MISTWEAVER,
        MONK_WINDWALKER,

        PALADIN_HOLY,
        PALADIN_PROTECTION,
        PALADIN_RETRIBUTION,

        PRIEST_HOLY,
        PRIEST_DISCIPLINE,
        PRIEST_SHADOW,

        ROGUE_SUBTLETY,
        ROGUE_ASSASSINATION,
        ROGUE_OUTLAW,

        SHAMAN_ENHANCEMENT,
        SHAMAN_RESTORATION,
        SHAMAN_ELEMENTAL,

        WARLOCK_AFFLICTION,
        WARLOCK_DEMONOLOGY,
        WARLOCK_DESTRUCTION,

        WARRIOR_ARMS,
        WARRIOR_FURY,
        WARRIOR_PROTECTION,

        TOTAL,
        UNKNOWN
    }

    public sealed class PlayerClassConv
    {
        public static string ClassToString(ClassSpec spec)
        {
            return spec switch
            {

                ClassSpec.DEMON_HUNTER_HAVOC => "Demon Hunter",
                ClassSpec.DEMON_HUNTER_VENGEANCE => "Demon Hunter",

                ClassSpec.DEATH_KNIGHT_UNHOLY => "Death Knight",
                ClassSpec.DEATH_KNIGHT_FROST => "Death Knight",
                ClassSpec.DEATH_KNIGHT_BLOOD => "Death Knight",

                ClassSpec.DRUID_GUARDIAN => "Druid",
                ClassSpec.DRUID_RESTORATION => "Druid",
                ClassSpec.DRUID_BALANCE => "Druid",
                ClassSpec.DRUID_FERAL => "Druid",

                ClassSpec.HUNTER_BEASTMASTERY => "Hunter",
                ClassSpec.HUNTER_MARKSMANSHIP => "Hunter",
                ClassSpec.HUNTER_SURVIVAL => "Hunter",

                ClassSpec.MAGE_ARCANE => "Mage",
                ClassSpec.MAGE_FROST => "Mage",
                ClassSpec.MAGE_FIRE => "Mage",

                ClassSpec.MONK_BREWMASTER => "Monk",
                ClassSpec.MONK_MISTWEAVER => "Monk",
                ClassSpec.MONK_WINDWALKER => "Monk",

                ClassSpec.PALADIN_HOLY => "Paladin",
                ClassSpec.PALADIN_PROTECTION => "Paladin",
                ClassSpec.PALADIN_RETRIBUTION => "Paladin",

                ClassSpec.PRIEST_HOLY => "Priest",
                ClassSpec.PRIEST_DISCIPLINE => "Priest",
                ClassSpec.PRIEST_SHADOW => "Priest",

                ClassSpec.ROGUE_SUBTLETY => "Rogue",
                ClassSpec.ROGUE_ASSASSINATION => "Rogue",
                ClassSpec.ROGUE_OUTLAW => "Rogue",

                ClassSpec.SHAMAN_ENHANCEMENT => "Shaman",
                ClassSpec.SHAMAN_RESTORATION => "Shaman",
                ClassSpec.SHAMAN_ELEMENTAL => "Shaman",

                ClassSpec.WARLOCK_AFFLICTION => "Warlock",
                ClassSpec.WARLOCK_DEMONOLOGY => "Warlock",
                ClassSpec.WARLOCK_DESTRUCTION => "Warlock",

                ClassSpec.WARRIOR_ARMS => "Warrior",
                ClassSpec.WARRIOR_FURY => "Warrior",
                ClassSpec.WARRIOR_PROTECTION => "Warrior",

                _  => "Unknown"
            };
        }

        public static string SpecToString(ClassSpec spec)
        {
            return spec switch
            {
                ClassSpec.DEMON_HUNTER_HAVOC => "Havoc",
                ClassSpec.DEMON_HUNTER_VENGEANCE => "Vengeance",

                ClassSpec.DEATH_KNIGHT_UNHOLY => "Unholy",
                ClassSpec.DEATH_KNIGHT_FROST => "Frost",
                ClassSpec.DEATH_KNIGHT_BLOOD => "Blood",

                ClassSpec.DRUID_GUARDIAN => "Guardian",
                ClassSpec.DRUID_RESTORATION => "Restoration",
                ClassSpec.DRUID_BALANCE => "Balance",
                ClassSpec.DRUID_FERAL => "Feral",

                ClassSpec.HUNTER_BEASTMASTERY => "Beastmastery",
                ClassSpec.HUNTER_MARKSMANSHIP => "Marksmanship",
                ClassSpec.HUNTER_SURVIVAL => "Survival",

                ClassSpec.MAGE_ARCANE => "Arcane",
                ClassSpec.MAGE_FROST => "Frost",
                ClassSpec.MAGE_FIRE => "Fire",

                ClassSpec.MONK_BREWMASTER => "Brewmaster",
                ClassSpec.MONK_MISTWEAVER => "Mistweaver",
                ClassSpec.MONK_WINDWALKER => "Windwalker",

                ClassSpec.PALADIN_HOLY => "Holy",
                ClassSpec.PALADIN_PROTECTION => "Protection",
                ClassSpec.PALADIN_RETRIBUTION => "Retribution",

                ClassSpec.PRIEST_HOLY => "Holy",
                ClassSpec.PRIEST_DISCIPLINE => "Discipline",
                ClassSpec.PRIEST_SHADOW => "Shadow",

                ClassSpec.ROGUE_SUBTLETY => "Subtlety",
                ClassSpec.ROGUE_ASSASSINATION => "Assassination",
                ClassSpec.ROGUE_OUTLAW => "Outlaw",

                ClassSpec.SHAMAN_ENHANCEMENT => "Enhancement",
                ClassSpec.SHAMAN_RESTORATION => "Restoration",
                ClassSpec.SHAMAN_ELEMENTAL => "Elemental",

                ClassSpec.WARLOCK_AFFLICTION => "Affliction",
                ClassSpec.WARLOCK_DEMONOLOGY => "Demonology",
                ClassSpec.WARLOCK_DESTRUCTION => "Destruction",

                ClassSpec.WARRIOR_ARMS => "Arms",
                ClassSpec.WARRIOR_FURY => "Fury",
                ClassSpec.WARRIOR_PROTECTION => "Protection",
                _  => "Unknown"
            };
        }
    }
}
