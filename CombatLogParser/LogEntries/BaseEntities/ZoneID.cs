﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public enum ZoneID
    {
        AshamanesFall = 1552,
        BlackRookHold = 1504,
        BladesEdge = 1672,
        Dalaran = 617,
        KulTiras = 1825,
        Mugambala = 1911,
        Nagrand = 1505,
        RuinsOfLordaeron = 572,
        TheRobodrome = 2167,
        TheTigersPeak = 1134,
        Tolviron = 980,
        EmpyreanDomain = 2373
    }

    public static class ZoneConverter
    {
        public static string ToString(int id)
        {
            return id switch
            {
                (int)ZoneID.AshamanesFall => "\"Ashamane's Fall\"",
                (int)ZoneID.BlackRookHold => "\"Black Rook Hold Arena\"",
                (int)ZoneID.BladesEdge => "\"Blade's Edge Arena\"",
                (int)ZoneID.Dalaran => "\"Dalaran Arena\"",
                (int)ZoneID.KulTiras => "\"Hook Point\"",
                (int)ZoneID.Mugambala => "\"Mugambala\"",
                (int)ZoneID.Nagrand => "\"Nagrand Arena\"",
                (int)ZoneID.RuinsOfLordaeron => "\"Ruins of Lordaeron\"",
                (int)ZoneID.TheRobodrome => "\"The Robodrome\"",
                (int)ZoneID.TheTigersPeak => "\"The Tiger's Peak\"",
                (int)ZoneID.Tolviron => "\"Tol'viron Arena\"",
                (int)ZoneID.EmpyreanDomain => "\"Empyrean Domain\"",
                _ => "Unknown arena"
            };
        }
    }

}
