﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries.BaseEntities
{
    public class CombatLogEntry : BaseLogEntry
    {
        public string CasterGUID { get; set; }
        public string TargetGUID { get; set; }

        public CombatLogEntry(): base() { }

        public CombatLogEntry(BaseLogEntry baseEntry, string casterGuid, string targetGuid) : base(baseEntry)
        {
            this.CasterGUID = casterGuid;
            this.TargetGUID = targetGuid;
        }

        public CombatLogEntry(CombatLogEntry other) : base(other.TimeStamp, other.Type)
        {
            CasterGUID = other.CasterGUID;
            TargetGUID = other.TargetGUID;
        }

        public CombatLogEntry(DateTime time, LogEntryType type) : base(time, type) { }

        public override string ToString()
        {
            return base.ToString() + ", Caster: " + this.CasterGUID + ", Target: " + this.TargetGUID;
        }
    }
}
