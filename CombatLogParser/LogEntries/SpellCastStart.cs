﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellCastStart : SpellLogEntry
    {
        public SpellCastStart() : base() { }

        public SpellCastStart(SpellLogEntry entry) : base(entry)
        {
        }
    }
}
