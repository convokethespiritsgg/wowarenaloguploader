﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class ArenaMatchEnd : EnvironmentLogEntry
    {
        public int WinnerTeamId { get; set; }
        public int DurationSeconds { get; set; }
        public int NewRatingTeam1 { get; set; }
        public int NewRatingTeam2 { get; set; }
        public ArenaMatchEnd() { }

        public ArenaMatchEnd(DateTime time, int winner, int duration, int newrating1, int newrating2) : base(time, LogEntryType.ARENA_MATCH_END)
        {
            WinnerTeamId = winner;
            DurationSeconds = duration;
            NewRatingTeam1 = newrating1;
            NewRatingTeam2 = newrating2;
        }
    }
}
