﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class ArenaMatchStart : EnvironmentLogEntry
    {
        public string Format { get; set; }
        public int ZoneId { get; set; }

        public ArenaMatchStart() { }

        public ArenaMatchStart(DateTime time, int zone, string format): base(time, LogEntryType.ARENA_MATCH_START)
        {
            ZoneId = zone;
            Format = format;
        }
    }
}
