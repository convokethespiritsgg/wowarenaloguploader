﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellSummon : SpellLogEntry
    {
        public SpellSummon() : base() { }

        public SpellSummon(SpellLogEntry baseEntry) : base(baseEntry)
        {
        }
    }
}
