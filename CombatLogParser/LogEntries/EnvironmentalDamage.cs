﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries
{
    public class EnvironmentalDamage: CombatLogEntry
    {
        public int Amount { get; set; }
        public int Absorbed { get; set; }
        public int OverKill { get; set; }
        public int TargetHPCurrent { get; set; }
        public int TargetHPMax { get; set; }
        public int TargetResouceCurrent { get; set; }
        public int TargetResourceMax { get; set; }
        public float TargetPositionX { get; set; }
        public float TargetPositionY { get; set; }
        public float TargetPositionZ { get; set; }
        public int TargetItemLevel { get; set; }
        public string Cause { get; set; }
        public bool IsCrit { get; set; }

        public EnvironmentalDamage(): base() { }

        public EnvironmentalDamage(CombatLogEntry entry, int hpCurrent, int hpMax, int resourceCurrent, int resourceMax, float posX, float posY, float posZ, int itemLvl, string cause, int amount, int overAmount, int absorbed, bool isCrit) : base(entry)
        {
            TargetHPMax = hpMax;
            TargetHPCurrent = hpCurrent;
            Amount = amount;
            Absorbed = absorbed;
            OverKill = overAmount;
            TargetResouceCurrent = resourceCurrent;
            TargetResourceMax = resourceMax;
            TargetPositionX = posX;
            TargetPositionY = posY;
            TargetPositionZ = posZ;
            TargetItemLevel = itemLvl;
            Cause = cause;
            IsCrit = isCrit;
        }

        public override string ToString()
        {
            return base.ToString() + ", TargetHP: " + TargetHPCurrent + "/" + TargetHPMax +
                                     ", TargetResouce: " + TargetResouceCurrent + "/" + TargetResourceMax +
                                     ", ilvl: " + TargetItemLevel +
                                     ", Cause: " + Cause +
                                     ", Amount: " + Amount +
                                     ", Absorbed: " + Absorbed +
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
