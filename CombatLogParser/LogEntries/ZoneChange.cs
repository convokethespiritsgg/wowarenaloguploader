﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class ZoneChange : EnvironmentLogEntry
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ZoneChange() { }

        public ZoneChange(DateTime time, int id, string name): base(time, LogEntryType.ZONE_CHANGE)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return base.ToString() + ", Id: " + Id + ", Name: " + Name;
        }
    }
}
