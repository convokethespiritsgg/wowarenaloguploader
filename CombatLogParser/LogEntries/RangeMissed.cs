﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries
{
    public class RangeMissed: SpellLogEntry
    {
        public LogEntryMissType MissType { get; set; }
        public bool IsOffHand { get; set; } // ¯\_(ツ)_/¯
        public int Absorbed { get; set; }
        public int InitialDamage { get; set; }
        public bool IsCrit { get; set; }

        public RangeMissed() : base() { }

        public RangeMissed(SpellLogEntry entry, LogEntryMissType missType, bool isOffHand, int amountAbsorbed, int initialDamage, bool isCrit) : base(entry)
        {
            MissType = missType;
            IsOffHand = isOffHand;
            Absorbed = amountAbsorbed;
            InitialDamage = initialDamage;
            IsCrit = isCrit;
        }

        public override string ToString()
        {
            return base.ToString() + ", MissType: " + MissType.ToString() + " (val: " + MissType + ")" +
                                     ", IsOffHand: " + IsOffHand +
                                     ", Absorbed: " + Absorbed +
                                     ", InitialDamage: " + InitialDamage +
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
