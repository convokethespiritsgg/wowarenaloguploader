﻿using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser.LogEntries
{
    public class SpellExtraAttacks: SpellLogEntry
    {
        public int ExtraAttacks { get; set; }

        public SpellExtraAttacks(SpellLogEntry entry, int extraAttacks): base(entry)
        {
            ExtraAttacks = extraAttacks;
        }

        public SpellExtraAttacks() : base() { }

        public override string ToString()
        {
            return base.ToString() + ", ExtraAttacks: " + ExtraAttacks;
        }
    }
}
