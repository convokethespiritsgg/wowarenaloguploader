﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SwingMissed : CombatLogEntry
    {
        public LogEntryMissType MissType { get; set; }
        public bool IsCrit { get; set; }
        public bool IsOffHand { get; set; }
        public int Absorbed { get; set; }
        public int InitialDamage { get; set; }

        public SwingMissed() : base() { }

        public SwingMissed(CombatLogEntry entry, LogEntryMissType missType, bool isOffHand, int amountAbsorbed, int  initialDamage, bool isCrit) : base(entry)
        {
            MissType = missType;
            IsOffHand = isOffHand;
            IsCrit = isCrit;
            Absorbed = amountAbsorbed;
            InitialDamage = initialDamage;
        }

        public override string ToString()
        {
            return base.ToString() + ", MissType: " + MissType.ToString() + " (val: " + MissType + ")" +
                                     ", IsOffHand: " + IsOffHand +
                                     ", Absorbed: " + Absorbed +
                                     ", InitialDamage: " + InitialDamage +
                                     (IsCrit ? ", Crit" : "");
        }
    }
}
