﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{

    public class SpellHealAbsorbed : CombatLogEntry
    {
        public string TargetHelperGUID { get; set; }
        public int TargetHelperSpellId { get; set; }
        public int Absorbed { get; set; }
        public int InitialHeal { get; set; }

        public SpellHealAbsorbed() : base() { }

        public SpellHealAbsorbed(SpellLogEntry entry, string targetHelperGuid, int targethelperSpellId, int absorbed, int initialHeal) : base(entry)
        {
            TargetHelperGUID = targetHelperGuid;
            TargetHelperSpellId = targethelperSpellId;
            Absorbed = absorbed;
            InitialHeal = initialHeal;
        }

        public override string ToString()
        {
            return base.ToString() + ", TargetHelperGUID: " + TargetHelperGUID + 
                                     ", TargetHelperSpellId: " + TargetHelperSpellId + 
                                     ", Absorbed: " + Absorbed + 
                                     ", InitialHeal: " + InitialHeal + "(Difference = " + (InitialHeal - Absorbed) + ")";
        }
    }
}
