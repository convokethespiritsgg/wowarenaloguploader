﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatLogParser.LogEntries.BaseEntities;

namespace CombatLogParser.LogEntries
{
    public class SpellAuraBrokenSpell : SpellLogEntry
    {
        public bool IsBuff { get; set; }
        public int HelperSpellId { get; set; }

        public SpellAuraBrokenSpell() : base() { }

        public SpellAuraBrokenSpell(SpellLogEntry entry, int helperSpellId, bool isBuff) : base(entry)
        {
            HelperSpellId = helperSpellId;
            IsBuff = isBuff;
        }

        public override string ToString()
        {
            return base.ToString() + ", HelperSpellId: " + HelperSpellId + (IsBuff ? ", Buff" : ", Debuff");
        }
    }
}
