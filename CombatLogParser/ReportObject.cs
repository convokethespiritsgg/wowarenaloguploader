﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using CombatLogParser;

namespace CombatLogParser
{
    public class ReportObject
    {
        public ReportKey ArenaParams { get; set; }

        public ReportData ArenaData { get; set; }

        public TimeZoneInfo TimeZone { get; set; }

        public ReportObject(ReportKey arenaParams, ReportData arenaData)
        {
            ArenaParams = arenaParams;
            ArenaData = arenaData;
            TimeZone = TimeZoneInfo.Local;
        }

        public string Serialize()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{");

            sb.Append("\"params\":" + JsonSerializer.Serialize<ReportKey>(ArenaParams, JsonSerializerConfig.opts));

            sb.Append(",\"data\": {");

            sb.Append(ArenaData.Serialize());

            sb.Append("},");

            sb.Append("\"timezone\":" + "\"" + TimeZone.ToSerializedString() + "\"");

            sb.Append("}");

            return sb.ToString();
        }

        public static ReportObject Deserialize(string serealized_data)
        {
            JsonDocument json_doc = JsonDocument.Parse(serealized_data);
            JsonElement root = json_doc.RootElement;

            ReportKey key = JsonSerializer.Deserialize<ReportKey>(root.GetProperty("params").ToString());
            ReportData data = ReportData.Deserialize(root.GetProperty("data"));

            TimeZoneInfo timezone = null;
            if (root.TryGetProperty("timezone", out JsonElement val))
            {
                timezone = TimeZoneInfo.FromSerializedString(val.ToString());
            }

            var rep_object = new ReportObject(key, data);
            rep_object.TimeZone = timezone;

            return rep_object;
        }

        public override string ToString()
        {
            return this.ArenaParams.ToString() + this.ArenaData.ToString();
        }
    }
}
