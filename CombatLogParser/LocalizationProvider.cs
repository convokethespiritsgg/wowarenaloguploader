﻿using CombatLogParser.Localization;
using CombatLogParser.Localization.LocalizedStrings;
using System;
using System.Collections.Generic;
using System.Text;

namespace CombatLogParser
{
    public class LocalizationProvider
    {
        private static readonly Dictionary<Locale, Dictionary<GlobalString, string>> locales = new Dictionary<Locale, Dictionary<GlobalString, string>>
        { 
            { Locale.deDE, German.LocalizedStrings },
            { Locale.enUS, English.LocalizedStrings },
            { Locale.esES, Spanish_Spain.LocalizedStrings },
            { Locale.esMX, Spanish_Latin_America.LocalizedStrings },
            { Locale.frFR, French.LocalizedStrings },
            { Locale.itIT, Italian.LocalizedStrings },
            { Locale.ptBR, Brazilian_Portuguese.LocalizedStrings },
            { Locale.ruRU, Russian.LocalizedStrings },
            { Locale.koKR, Korean.LocalizedStrings },
            { Locale.zhCN, Simplified_Chinese.LocalizedStrings },
            { Locale.zhTW, Traditional_Chinese.LocalizedStrings }
        };
        public static LocalizedArray LoadLocalization(Locale loc)
        {
            LocalizedArray arr = new LocalizedArray(loc, locales[loc]);
            return arr;
        }
    }
}
