﻿using CombatLogParser.Localization;
using CombatLogParser.Localization.SpecialCases;
using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using CombatLogParser.LogEntries.Covenant;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


#if NET48
// https://github.com/dotnet/runtime/blob/419e949d258ecee4c40a460fb09c66d974229623/src/libraries/System.Private.CoreLib/src/System/Index.cs
// https://github.com/dotnet/runtime/blob/419e949d258ecee4c40a460fb09c66d974229623/src/libraries/System.Private.CoreLib/src/System/Range.cs

using System.Runtime.CompilerServices;

namespace System
{
    /// <summary>Represent a type can be used to index a collection either from the start or the end.</summary>
    /// <remarks>
    /// Index is used by the C# compiler to support the new index syntax
    /// <code>
    /// int[] someArray = new int[5] { 1, 2, 3, 4, 5 } ;
    /// int lastElement = someArray[^1]; // lastElement = 5
    /// </code>
    /// </remarks>
    internal readonly struct Index : IEquatable<Index>
    {
        private readonly int _value;

        /// <summary>Construct an Index using a value and indicating if the index is from the start or from the end.</summary>
        /// <param name="value">The index value. it has to be zero or positive number.</param>
        /// <param name="fromEnd">Indicating if the index is from the start or from the end.</param>
        /// <remarks>
        /// If the Index constructed from the end, index value 1 means pointing at the last element and index value 0 means pointing at beyond last element.
        /// </remarks>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Index(int value, bool fromEnd = false)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "value must be non-negative");
            }

            if (fromEnd)
                _value = ~value;
            else
                _value = value;
        }

        // The following private constructors mainly created for perf reason to avoid the checks
        private Index(int value)
        {
            _value = value;
        }

        /// <summary>Create an Index pointing at first element.</summary>
        public static Index Start => new Index(0);

        /// <summary>Create an Index pointing at beyond last element.</summary>
        public static Index End => new Index(~0);

        /// <summary>Create an Index from the start at the position indicated by the value.</summary>
        /// <param name="value">The index value from the start.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Index FromStart(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "value must be non-negative");
            }

            return new Index(value);
        }

        /// <summary>Create an Index from the end at the position indicated by the value.</summary>
        /// <param name="value">The index value from the end.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Index FromEnd(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "value must be non-negative");
            }

            return new Index(~value);
        }

        /// <summary>Returns the index value.</summary>
        public int Value
        {
            get
            {
                if (_value < 0)
                {
                    return ~_value;
                }
                else
                {
                    return _value;
                }
            }
        }

        /// <summary>Indicates whether the index is from the start or the end.</summary>
        public bool IsFromEnd => _value < 0;

        /// <summary>Calculate the offset from the start using the giving collection length.</summary>
        /// <param name="length">The length of the collection that the Index will be used with. length has to be a positive value</param>
        /// <remarks>
        /// For performance reason, we don't validate the input length parameter and the returned offset value against negative values.
        /// we don't validate either the returned offset is greater than the input length.
        /// It is expected Index will be used with collections which always have non negative length/count. If the returned offset is negative and
        /// then used to index a collection will get out of range exception which will be same affect as the validation.
        /// </remarks>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetOffset(int length)
        {
            var offset = _value;
            if (IsFromEnd)
            {
                // offset = length - (~value)
                // offset = length + (~(~value) + 1)
                // offset = length + value + 1

                offset += length + 1;
            }
            return offset;
        }

        /// <summary>Indicates whether the current Index object is equal to another object of the same type.</summary>
        /// <param name="value">An object to compare with this object</param>
        public override bool Equals(object? value) => value is Index && _value == ((Index)value)._value;

        /// <summary>Indicates whether the current Index object is equal to another Index object.</summary>
        /// <param name="other">An object to compare with this object</param>
        public bool Equals(Index other) => _value == other._value;

        /// <summary>Returns the hash code for this instance.</summary>
        public override int GetHashCode() => _value;

        /// <summary>Converts integer number to an Index.</summary>
        public static implicit operator Index(int value) => FromStart(value);

        /// <summary>Converts the value of the current Index object to its equivalent string representation.</summary>
        public override string ToString()
        {
            if (IsFromEnd)
                return "^" + ((uint)Value).ToString();

            return ((uint)Value).ToString();
        }
    }

    /// <summary>Represent a range has start and end indexes.</summary>
    /// <remarks>
    /// Range is used by the C# compiler to support the range syntax.
    /// <code>
    /// int[] someArray = new int[5] { 1, 2, 3, 4, 5 };
    /// int[] subArray1 = someArray[0..2]; // { 1, 2 }
    /// int[] subArray2 = someArray[1..^0]; // { 2, 3, 4, 5 }
    /// </code>
    /// </remarks>
    internal readonly struct Range : IEquatable<Range>
    {
        /// <summary>Represent the inclusive start index of the Range.</summary>
        public Index Start { get; }

        /// <summary>Represent the exclusive end index of the Range.</summary>
        public Index End { get; }

        /// <summary>Construct a Range object using the start and end indexes.</summary>
        /// <param name="start">Represent the inclusive start index of the range.</param>
        /// <param name="end">Represent the exclusive end index of the range.</param>
        public Range(Index start, Index end)
        {
            Start = start;
            End = end;
        }

        /// <summary>Indicates whether the current Range object is equal to another object of the same type.</summary>
        /// <param name="value">An object to compare with this object</param>
        public override bool Equals(object? value) =>
            value is Range r &&
            r.Start.Equals(Start) &&
            r.End.Equals(End);

        /// <summary>Indicates whether the current Range object is equal to another Range object.</summary>
        /// <param name="other">An object to compare with this object</param>
        public bool Equals(Range other) => other.Start.Equals(Start) && other.End.Equals(End);

        /// <summary>Returns the hash code for this instance.</summary>
        public override int GetHashCode()
        {
            return Start.GetHashCode() * 31 + End.GetHashCode();
        }

        /// <summary>Converts the value of the current Range object to its equivalent string representation.</summary>
        public override string ToString()
        {
            return Start + ".." + End;
        }

        /// <summary>Create a Range object starting from start index to the end of the collection.</summary>
        public static Range StartAt(Index start) => new Range(start, Index.End);

        /// <summary>Create a Range object starting from first element in the collection to the end Index.</summary>
        public static Range EndAt(Index end) => new Range(Index.Start, end);

        /// <summary>Create a Range object starting from first element to the end.</summary>
        public static Range All => new Range(Index.Start, Index.End);

        /// <summary>Calculate the start offset and length of range object using a collection length.</summary>
        /// <param name="length">The length of the collection that the range will be used with. length has to be a positive value.</param>
        /// <remarks>
        /// For performance reason, we don't validate the input length parameter against negative values.
        /// It is expected Range will be used with collections which always have non negative length/count.
        /// We validate the range is inside the length scope though.
        /// </remarks>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public (int Offset, int Length) GetOffsetAndLength(int length)
        {
            int start;
            var startIndex = Start;
            if (startIndex.IsFromEnd)
                start = length - startIndex.Value;
            else
                start = startIndex.Value;

            int end;
            var endIndex = End;
            if (endIndex.IsFromEnd)
                end = length - endIndex.Value;
            else
                end = endIndex.Value;

            if ((uint)end > (uint)length || (uint)start > (uint)end)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            return (start, end - start);
        }
    }
}

namespace System.Runtime.CompilerServices
{
    internal static class RuntimeHelpers
    {
        /// <summary>
        /// Slices the specified array using the specified range.
        /// </summary>
        public static T[] GetSubArray<T>(T[] array, Range range)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            (int offset, int length) = range.GetOffsetAndLength(array.Length);

            if (default(T) != null || typeof(T[]) == array.GetType())
            {
                // We know the type of the array to be exactly T[].

                if (length == 0)
                {
                    return Array.Empty<T>();
                }

                var dest = new T[length];
                Array.Copy(array, offset, dest, 0, length);
                return dest;
            }
            else
            {
                // The array is actually a U[] where U:T.
                var dest = (T[])Array.CreateInstance(array.GetType().GetElementType(), length);
                Array.Copy(array, offset, dest, 0, length);
                return dest;
            }
        }
    }
}
#endif

namespace CombatLogParser
{
    public class Parser
    {
        public static readonly int Version = 1;
        public Dictionary<string, LogSubject> Subjects { get; }
        public Dictionary<int, LogSpell> Spells { get; }
        public string Issuer { get; private set; }

        private TimeZoneInfo TimeZone = TimeZoneInfo.Local;
        private LocalizedArray localization = LocalizationProvider.LoadLocalization(Locale.enUS);

        public Parser()
        {
            Subjects = new Dictionary<string, LogSubject>();
            Spells = new Dictionary<int, LogSpell>();

            InitializeSpells();
        }

        public Parser(TimeZoneInfo timeZone): this()
        {
            TimeZone = timeZone;
        }


        // Not sure that we should ever clear spells;
        public void Prepare()
        {
            Subjects.Clear();
            Spells.Clear();
            Issuer = null;
            InitializeSpells();
        }

        public void SetLocale(string locale)
        {
            Locale loc = LocaleString.Parse(locale);
            localization = LocalizationProvider.LoadLocalization(loc);
        }

        public Locale GetLocale()
        {
            return localization.GetLocale();
        }

        private void InitializeSpells()
        {
            Spells.Add(LogSpellConst.WHITE_MELEE_ATTACK, new LogSpell(LogSpellConst.WHITE_MELEE_ATTACK, LogEntrySchoolType.PHYSICAL));
            Spells.Add(LogSpellConst.WHITE_RANGE_ATTACK, new LogSpell(LogSpellConst.WHITE_RANGE_ATTACK, LogEntrySchoolType.PHYSICAL));
        }

        // This method provides more complex parsing. The main issue was that the parameters split by commas, but string parameters themselves could also have commas
        private string[] ParseRawParams(string rawString)
        {
            List<string> parameters = new List<string>();
            bool inString = false;
            int startIdx = 0, endIdx = 0;
            for (; endIdx < rawString.Length; endIdx++)
            {
                if (rawString[endIdx] == '"')
                {
                    inString = !inString;
                    continue;
                }
                if (rawString[endIdx] == ',' && !inString)
                {
                    parameters.Add(rawString[startIdx..endIdx]);
                    startIdx = endIdx + 1;
                }
            }
            parameters.Add(rawString[startIdx..endIdx]);
            return parameters.ToArray();
        }

        private void ParseLogTime(string rawLogEntry, out DateTime date, out string restOfLogEntry)
        {
            int pos = rawLogEntry.IndexOf("  ");
            if (pos != -1)
            {
                string dateRaw = rawLogEntry[0..pos];
                date = TimeZoneInfo.ConvertTimeToUtc(DateTime.ParseExact(dateRaw, "M/d HH:mm:ss.fff", CultureInfo.InvariantCulture), TimeZone);
                restOfLogEntry = rawLogEntry[(pos + 1)..];
            }
            else
            {
                throw new ParseFailedException("Can't parse time");
            }
        }

        public BaseLogEntry ParseLogEntry(string rawLogEntry)
        {
            try
            {
                ParseLogTime(rawLogEntry, out DateTime timeStamp, out rawLogEntry);
                return ParseLogEntryFromParams(timeStamp, ParseRawParams(rawLogEntry));
            }
            catch (Exception e)
            {
                return BaseLogEntry.InvalidEntry(rawLogEntry, e.Message + '\n' + e.StackTrace + '\n' + e.Source);
            }

        }

        private BaseLogEntry ParseLogEntryFromParams(DateTime timeStamp, string[] parameters)
        {
            LogEntryType type = LogEntryTypeConv.FromString(parameters[0].Trim());
            BaseLogEntry baseEntry = new BaseLogEntry(timeStamp, type);

            if (LogEntryTypeConv.LogEntryIsCombat(type))
            {
                // get rid of event type parameter;
                parameters = parameters[1..];

                LogSubject caster = ParseLogSubject(parameters);
                parameters = parameters[4..]; // Offset to target fields

                LogSubject target = ParseLogSubject(parameters);
                parameters = parameters[4..];

                // Take only the guid
                CombatLogEntry combatEntry = new CombatLogEntry(baseEntry, caster.GUID, target.GUID);

                if (LogEntryTypeConv.LogEntryIsSpell(type))
                {

                    if (type == LogEntryType.SPELL_ABSORBED)
                    {
                        LogSpell _spell = Spells[LogSpellConst.WHITE_MELEE_ATTACK];
                        /* 
                         * SPELL_ABSORBED has a variable number of parameters.
                         * If damage type is a simple attack, then it doesn't present in log and we can check this by 
                         */
                        if (parameters.Length > 10)
                        {
                            _spell = ParseLogSpell(parameters);
                            parameters = parameters[3..];
                        }

                        return ParseSpellAbsorbed(combatEntry, _spell, parameters);
                    }

                    LogSpell spell = ParseLogSpell(parameters);
                    parameters = parameters[3..]; // offset to other fields

                    // Take only the spell ID
                    SpellLogEntry spellLogEntry = new SpellLogEntry(combatEntry, spell.SpellId);

                    if (parameters.Length == 0)
                    {
                        if (spellLogEntry.Type == LogEntryType.SPELL_SUMMON)
                            return new SpellSummon(spellLogEntry);
                        else if (spellLogEntry.Type == LogEntryType.SPELL_CREATE)
                            return new SpellCreate(spellLogEntry);
                    }

                    // ASSUME THAT WE STARTS PARSING OTHER PARAMETERS FROM 12 INDEX (13th parameter)
                    // At this point we have parsed the caster subject, the target subject and the spell entry

                    switch (spellLogEntry.Type)
                    {
                        case LogEntryType.SPELL_INSTAKILL:
                            return ParseSpellInstakillEvent(spellLogEntry, parameters);
                        case LogEntryType.SPELL_STOLEN:
                            return ParseSpellStolen(spellLogEntry, parameters);
                        case LogEntryType.SPELL_INTERRUPT:
                            return ParseSpellInterrupt(spellLogEntry, parameters);
                        case LogEntryType.SPELL_DISPEL:
                            return ParseSpellDispel(spellLogEntry, parameters);
                        case LogEntryType.SPELL_DISPEL_FAILED:
                            return ParseSpellDispelFailed(spellLogEntry, parameters);

                        case LogEntryType.SPELL_DRAIN:
                        case LogEntryType.SPELL_ENERGIZE:
                        case LogEntryType.SPELL_PERIODIC_ENERGIZE:
                        case LogEntryType.SPELL_DAMAGE:
                        case LogEntryType.SPELL_PERIODIC_DAMAGE:
                        case LogEntryType.SPELL_HEAL:
                        case LogEntryType.SPELL_PERIODIC_HEAL:
                        case LogEntryType.DAMAGE_SPLIT:
                        case LogEntryType.RANGE_DAMAGE:
                            return ParseSpellResourcesEvent(spellLogEntry, parameters);

                        case LogEntryType.SPELL_HEAL_ABSORBED:
                            return ParseSpellHealAbsorbed(spellLogEntry, parameters);

                        case LogEntryType.RANGE_MISSED:
                        case LogEntryType.SPELL_MISSED:
                        case LogEntryType.SPELL_PERIODIC_MISSED:
                            return ParseSpellMissed(spellLogEntry, parameters);

                        case LogEntryType.SPELL_AURA_APPLIED:
                        case LogEntryType.SPELL_AURA_APPLIED_DOSE:
                        case LogEntryType.SPELL_AURA_REMOVED:
                        case LogEntryType.SPELL_AURA_REMOVED_DOSE:
                        case LogEntryType.SPELL_AURA_REFRESH:
                        case LogEntryType.SPELL_AURA_BROKEN:
                        case LogEntryType.SPELL_AURA_BROKEN_SPELL:
                            return ParseSpellAura(spellLogEntry, parameters);

                        case LogEntryType.SPELL_CAST_SUCCESS:
                        case LogEntryType.SPELL_CAST_FAILED:
                        case LogEntryType.SPELL_CAST_START:
                            return ParseSpellCast(spellLogEntry, parameters);

                        case LogEntryType.SPELL_EXTRA_ATTACKS:
                            return ParseSpellExtraAttacks(spellLogEntry, parameters);
                        default:
                            break;
                    }

                }
                else
                {
                    // Combat event, but not the spell
                    // ASSUME THAT WE STARTS PARSING OTHER PARAMETERS FROM 9 INDEX (10th parameter)
                    // At this point we have parsed the source subject and the target subject

                    switch (type)
                    {
                        case LogEntryType.UNIT_DIED:
                            return ParseUnitDied(combatEntry, parameters);
                        case LogEntryType.UNIT_DESTROYED:
                            return ParseUnitDestroyed(combatEntry, parameters);
                        case LogEntryType.SWING_MISSED:
                            return ParseSwingMissed(combatEntry, parameters);
                        case LogEntryType.SWING_DAMAGE_LANDED:
                        case LogEntryType.SWING_DAMAGE:
                            return ParseDamage(combatEntry, type, parameters);
                        case LogEntryType.PARTY_KILL:
                            return ParsePartyKill(combatEntry, parameters);
                        case LogEntryType.ENVIRONMENTAL_DAMAGE:
                            return ParseEnvironmentalDamageEvent(combatEntry, parameters);
                        default:
                            break;
                    }
                }
            }
            else
            {
                // Environmental events
                switch (type)
                {
                    case LogEntryType.COMBAT_LOG_VERSION:
                        return ParseCombatLogVersion(timeStamp, parameters);
                    case LogEntryType.ZONE_CHANGE:
                        return ParseZoneChange(timeStamp, parameters);
                    case LogEntryType.ARENA_MATCH_START:
                        return ParseArenaMatchStart(timeStamp, parameters);
                    case LogEntryType.ARENA_MATCH_END:
                        return ParseArenaMatchEnd(timeStamp, parameters);
                    case LogEntryType.COMBATANT_INFO:
                        return ParseCombatantInfo(timeStamp, parameters);
                    default:
                        break;
                }
            }

            throw new ParseFailedException("Can't parse event from parameters array");
        }

        private BaseLogEntry ParseSpellInstakillEvent(SpellLogEntry spellLogEntry, string[] parameters)
        {
            int field = int.Parse(parameters[0]);

            return new SpellInstakill(spellLogEntry, field);
        }

        private BaseLogEntry ParseEnvironmentalDamageEvent(CombatLogEntry entry, string[] parameters)
        {
            // 9 => 0
            int hpCurrent = int.Parse(parameters[2]);        // 11
            int hpMax = int.Parse(parameters[3]);            // 12
            int resourceCurrent = int.Parse(parameters[9]);  // 18
            int resourceMax = int.Parse(parameters[10]);     // 19
            float posX = float.Parse(parameters[12], CultureInfo.InvariantCulture); // 21
            float posY = float.Parse(parameters[13], CultureInfo.InvariantCulture); // 22
            float posZ = float.Parse(parameters[14], CultureInfo.InvariantCulture); // 23
            int itemLvl = int.Parse(parameters[16]);         // 25
            string cause = parameters[17];
            int amount = int.Parse(parameters[18]);          // 27
            int overAmount = int.Parse(parameters[19]);      // 28
            int absorbed = int.Parse(parameters[23]);
            bool isCrit = !parameters[24].Contains("nil");   // 34

            return new EnvironmentalDamage(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, cause, amount, overAmount, absorbed, isCrit);
        }

        private BaseLogEntry ParseSpellExtraAttacks(SpellLogEntry spellLogEntry, string[] parameters)
        {
            int extraAttacks = int.Parse(parameters[0]);

            return new SpellExtraAttacks(spellLogEntry, extraAttacks);
        }

        private BaseLogEntry ParseUnitDestroyed(CombatLogEntry entry, string[] parameters)
        {
            int field = int.Parse(parameters[0]);

            return new UnitDestroyed(entry, field);
        }

        private BaseLogEntry ParseCombatLogVersion(DateTime timeStamp, string[] parameters)
        {
            int version = int.Parse(parameters[1]);
            int adv_log = int.Parse(parameters[3]);
            if (adv_log == 0)
                return new CombatLogVersion(timeStamp, version, false, "", 0);

            string wow_build = parameters[5];
            int project_id = int.Parse(parameters[7]);

            return new CombatLogVersion(timeStamp, version, true, wow_build, project_id);
        }

        private BaseLogEntry ParseSpellResourcesEvent(SpellLogEntry entry, string[] parameters)
        {
            // 12 => 0
            int hpCurrent       = int.Parse(parameters[2]);  // 14
            int hpMax           = int.Parse(parameters[3]);  // 15
            int resourceCurrent = int.Parse(parameters[9]);  // 21
            int resourceMax     = int.Parse(parameters[10]); // 22
            float posX          = float.Parse(parameters[12], CultureInfo.InvariantCulture); // 24
            float posY          = float.Parse(parameters[13], CultureInfo.InvariantCulture); // 25
            float posZ          = float.Parse(parameters[14], CultureInfo.InvariantCulture); // 26
            int itemLvl         = int.Parse(parameters[16]); // 28
            float amount        = float.Parse(parameters[17], CultureInfo.InvariantCulture.NumberFormat);  // 29
            float overAmount    = float.Parse(parameters[18], CultureInfo.InvariantCulture.NumberFormat);  // 30
            bool isCrit;


            if (entry.Type == LogEntryType.SPELL_DRAIN)
                return new SpellDrain(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount);
            else if (entry.Type == LogEntryType.SPELL_ENERGIZE)
                return new SpellEnergize(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount);
            else if (entry.Type == LogEntryType.SPELL_PERIODIC_ENERGIZE)
                return new SpellPeriodicEnergize(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount);
            else if (entry.Type == LogEntryType.SPELL_HEAL || entry.Type == LogEntryType.SPELL_PERIODIC_HEAL)
            {
                isCrit = !parameters[21].Contains("nil"); // 33

                if (entry.Type == LogEntryType.SPELL_HEAL)
                    return new SpellHeal(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit);
                else if (entry.Type == LogEntryType.SPELL_PERIODIC_HEAL)
                    return new SpellPeriodicHeal(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit);
            }
            else if (entry.Type == LogEntryType.SPELL_DAMAGE || entry.Type == LogEntryType.SPELL_PERIODIC_DAMAGE || entry.Type == LogEntryType.RANGE_DAMAGE || entry.Type == LogEntryType.DAMAGE_SPLIT)
            {
                int absorbed = int.Parse(parameters[23]);
                isCrit = !parameters[24].Contains("nil"); // 36

                if (entry.Type == LogEntryType.SPELL_DAMAGE)
                {
                    return new SpellDamage(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit, absorbed);
                }
                else if (entry.Type == LogEntryType.SPELL_PERIODIC_DAMAGE)
                {
                    return new SpellPeriodicDamage(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit, absorbed);
                }
                else if (entry.Type == LogEntryType.DAMAGE_SPLIT)
                {
                    return new DamageSplit(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit, absorbed);
                }
                else if (entry.Type == LogEntryType.RANGE_DAMAGE)
                {
                    return new RangeDamage(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, isCrit, absorbed);
                }
            }

            throw new ParseFailedException("Cannot compare to appropriate spell resource event");
        }

        private BaseLogEntry ParsePartyKill(CombatLogEntry entry, string[] parameters)
        {
            string unknown = parameters[0];
            return new PartyKill(entry, unknown);
        }

        private BaseLogEntry ParseSpellStolen(SpellLogEntry entry, string[] parameters)
        {
            LogSpell spell = this.ParseLogSpell(parameters);

            bool IsBuff = !parameters[3].Contains("DEBUFF");

            return new SpellStolen(entry, spell.SpellId, IsBuff);
        }

        private BaseLogEntry ParseSpellHealAbsorbed(SpellLogEntry entry, string[] parameters)
        {
            int index = 0;
            LogSubject targetHelper = ParseLogSubject(parameters[index..(index + 4)]);
            index += 4;
            LogSpell targethelperSpell = this.ParseLogSpell(parameters[index..(index + 3)]);
            index += 3;

            int absorbed = int.Parse(parameters[index++]);
            int initialHeal = int.Parse(parameters[index++]);

            return new SpellHealAbsorbed(entry, targetHelper.GUID, targethelperSpell.SpellId, absorbed, initialHeal);
        }

        private BaseLogEntry ParseSpellInterrupt(SpellLogEntry entry, string[] parameters)
        {
            LogSpell spell = this.ParseLogSpell(parameters);

            return new SpellInterrupt(entry, spell.SpellId);
        }

        private BaseLogEntry ParseSpellDispelFailed(SpellLogEntry entry, string[] parameters)
        {
            LogSpell spell = this.ParseLogSpell(parameters);

            return new SpellDispelFailed(entry, spell.SpellId);
        }

        private BaseLogEntry ParseSpellDispel(SpellLogEntry entry, string[] parameters)
        {
            LogSpell spell = this.ParseLogSpell(parameters);

            bool IsBuff = !parameters[3].Contains("DEBUFF");

            return new SpellDispel(entry, spell.SpellId, IsBuff);
        }


        private BaseLogEntry ParseSpellMissed(SpellLogEntry entry, string[] parameters)
        {
            // Starts from 12 parameter
            LogEntryMissType missType = LogEntryTypeConv.MissFromString(parameters[0]);
            bool isOffHand = !parameters[1].Contains("nil");
            int amountAbsorbed = 0;
            int initialDamage = 0;
            bool isCrit = false;
            if (missType == LogEntryMissType.MISS_ABSORB)
            {
                amountAbsorbed = int.Parse(parameters[2]);
                initialDamage = int.Parse(parameters[3]);
                isCrit = !parameters[4].Contains("nil");
            }
            
            if (entry.Type == LogEntryType.SPELL_MISSED)
                return new SpellMissed(entry, missType, isOffHand, amountAbsorbed, initialDamage, isCrit);
            else if (entry.Type == LogEntryType.SPELL_PERIODIC_MISSED)
                return new SpellPeriodicMissed(entry, missType, isOffHand, amountAbsorbed, initialDamage, isCrit);
            else if (entry.Type == LogEntryType.RANGE_MISSED)
                return new RangeMissed(entry, missType, isOffHand, amountAbsorbed, initialDamage, isCrit);

            throw new ParseFailedException("Cannot compare to appropriate resource event");
        }

        private BaseLogEntry ParseSpellAbsorbed(CombatLogEntry combatEntry, LogSpell attackSpell, string[] parameters)
        {
            LogSubject targetHelper = ParseLogSubject(parameters);
            parameters = parameters[4..];
            LogSpell absorbSpell = ParseLogSpell(parameters);
            parameters = parameters[3..];

            int AbsorbedAmount = int.Parse(parameters[0]);
            int InitialDamage = int.Parse(parameters[1]);
            bool isCrit = !parameters[2].Contains("nil");

            return new SpellAbsorbed(combatEntry, targetHelper.GUID, absorbSpell.SpellId, AbsorbedAmount, attackSpell.SpellId, InitialDamage, isCrit);
        }

        private SwingMissed ParseSwingMissed(CombatLogEntry entry, string[] parameters)
        {
            // 9 => 0
            int index = 0;
            // parsing parameters
            bool isOffHand = false;
            int amountAbsorbed = 0;
            int initialDamage = 0;

            LogEntryMissType missType = LogEntryTypeConv.MissFromString(parameters[index++]);
            if (missType == LogEntryMissType.MISS_ABSORB)
            {
                isOffHand = !parameters[index++].Contains("nil");
                amountAbsorbed = int.Parse(parameters[index++]);
                initialDamage = int.Parse(parameters[index++]);
            }
            bool isCrit = !parameters[index].Contains("nil");

            return new SwingMissed(entry, missType, isOffHand, amountAbsorbed, initialDamage, isCrit);
        }

        private BaseLogEntry ParseSpellCast(SpellLogEntry entry, string[] parameters)
        {
            if (entry.Type == LogEntryType.SPELL_CAST_START)
                return new SpellCastStart(entry);

            if (entry.Type == LogEntryType.SPELL_CAST_FAILED)
            {
                if (Issuer == null && Subjects.ContainsKey(entry.CasterGUID))
                {
                    var subj = Subjects[entry.CasterGUID];
                    Issuer = subj.Name;
                }

                string reason = parameters[0].Replace("\"","");
#if false // Turned off until localization is needed
                GlobalString unified_reason = localization.GetByStringValue(reason);
                if (unified_reason == GlobalString.UNKNOWN_GLOBAL_STRING)
                {
                    var timeStamp = DateTime.Now.ToString("HH:mm:ss");
                    System.Diagnostics.Debug.WriteLine("{0} Parse failed: \"{1}\" to \"{2}\"", timeStamp, reason, unified_reason);
                }
                if (unified_reason == GlobalString.SPELL_FAILED_PREVENTED_BY_MECHANIC)
                {
                    string detailedReason = SpellPreventedByMechanicParser.Parse(reason, localization.GetLocale());

                    return new SpellCastFailed(entry, unified_reason, detailedReason);
                }
                return new SpellCastFailed(entry, unified_reason);
#else
                return new SpellCastFailed(entry, GlobalString.UNKNOWN_GLOBAL_STRING, reason);
#endif
            }

            if (entry.Type == LogEntryType.SPELL_CAST_SUCCESS)
            {
                string spellTargetGuid = parameters[0];
                float posX = float.Parse(parameters[12], CultureInfo.InvariantCulture);
                float posY = float.Parse(parameters[13], CultureInfo.InvariantCulture);
                float posZ = float.Parse(parameters[14], CultureInfo.InvariantCulture);
                int itemLvl = int.Parse(parameters[16]);
                return new SpellCastSuccess(entry, spellTargetGuid, posX, posY, posZ, itemLvl);
            }

            throw new ParseFailedException("Cannot compare to appropriate spell cast event");
        }

        private ZoneChange ParseZoneChange(DateTime timeStamp, string[] parameters)
        {
            int id = int.Parse(parameters[1]);
            string zoneName = parameters[2];

            return new ZoneChange(timeStamp, id, zoneName);
        }

        private BaseLogEntry ParseDamage(CombatLogEntry entry, LogEntryType type, string[] parameters)
        {
            // 9 => 0
            int hpCurrent = int.Parse(parameters[2]);        // 11
            int hpMax = int.Parse(parameters[3]);            // 12
            int resourceCurrent = int.Parse(parameters[9]);  // 18
            int resourceMax = int.Parse(parameters[10]);     // 19
            float posX = float.Parse(parameters[12], CultureInfo.InvariantCulture); // 21
            float posY = float.Parse(parameters[13], CultureInfo.InvariantCulture); // 22
            float posZ = float.Parse(parameters[14], CultureInfo.InvariantCulture); // 23
            int itemLvl = int.Parse(parameters[16]);         // 25
            int amount = int.Parse(parameters[17]);          // 26
            int overAmount = int.Parse(parameters[18]);      // 27
            int absorbed = int.Parse(parameters[23]);
            bool isCrit = !parameters[24].Contains("nil");   // 33

            if (type == LogEntryType.SWING_DAMAGE)
                return new SwingDamage(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, absorbed, isCrit);
            else if (type == LogEntryType.SWING_DAMAGE_LANDED)
                return new SwingDamageLanded(entry, hpCurrent, hpMax, resourceCurrent, resourceMax, posX, posY, posZ, itemLvl, amount, overAmount, absorbed, isCrit);

            throw new ParseFailedException("Cannot compare to appropriate swing damage event");
        }

        private BaseLogEntry ParseSpellAura(SpellLogEntry entry, string[] parameters)
        {
            // again, from 12 index in initial parameters
            int helperSpellId = LogSpellConst.INVALID_SPELL_ID; // for SPELL_AURA_BROKEN_SPELL
            int index = 0;
            int amount = 1;

            if (entry.Type == LogEntryType.SPELL_AURA_BROKEN_SPELL)
            {
                LogSpell helperSpell = this.ParseLogSpell(parameters);
                helperSpellId = helperSpell.SpellId;

                index += 3;
            }

            bool isBuff = !parameters[index++].Contains("DEBUFF");

            if (parameters.Length > index)
                amount = int.Parse(parameters[index]);

            if (entry.Type == LogEntryType.SPELL_AURA_APPLIED)
                return new SpellAuraApplied(entry, isBuff);
            else if (entry.Type == LogEntryType.SPELL_AURA_REMOVED)
                return new SpellAuraRemoved(entry, isBuff);
            else if (entry.Type == LogEntryType.SPELL_AURA_REFRESH)
                return new SpellAuraRefresh(entry, isBuff);
            else if (entry.Type == LogEntryType.SPELL_AURA_BROKEN)
                return new SpellAuraBroken(entry, isBuff);

            if (entry.Type == LogEntryType.SPELL_AURA_APPLIED_DOSE)
                return new SpellAuraAppliedDose(entry, isBuff, amount);
            else if (entry.Type == LogEntryType.SPELL_AURA_REMOVED_DOSE)
                return new SpellAuraRemovedDose(entry, isBuff, amount);

            if (entry.Type == LogEntryType.SPELL_AURA_BROKEN_SPELL)
                return new SpellAuraBrokenSpell(entry, helperSpellId, isBuff);

            throw new ParseFailedException("Cannot compare to appropriate spell aura event");
        }

        private ArenaMatchStart ParseArenaMatchStart(DateTime timeStamp, string[] parameters)
        {
            int zone = int.Parse(parameters[1]);
            string format = parameters[3];
            return new ArenaMatchStart(timeStamp, zone, format);
        }

        private ArenaMatchEnd ParseArenaMatchEnd(DateTime timeStamp, string [] parameters)
        {
            int winner = int.Parse(parameters[1]);
            int duration = int.Parse(parameters[2]);
            int newrating1 = int.Parse(parameters[3]);
            int newrating2 = int.Parse(parameters[4]);
            return new ArenaMatchEnd(timeStamp, winner, duration, newrating1, newrating2);
        }

        private CombatantInfo ParseCombatantInfo(DateTime timeStamp, string[] parameters)
        {
            string guid = parameters[1];
            int team_number = int.Parse(parameters[2]);
            int specId = int.Parse(parameters[24]);

            ClassSpec _spec = ClassSpec.UNKNOWN;

            switch (specId)
            {
                case 62:
                    _spec = ClassSpec.MAGE_ARCANE;
                    break;
                case 63:
                    _spec = ClassSpec.MAGE_FIRE;
                    break;
                case 64:
                    _spec = ClassSpec.MAGE_FROST;
                    break;
                case 65:
                    _spec = ClassSpec.PALADIN_HOLY;
                    break;
                case 66:
                    _spec = ClassSpec.PALADIN_PROTECTION;
                    break;
                case 70:
                    _spec = ClassSpec.PALADIN_RETRIBUTION;
                    break;
                case 71:
                    _spec = ClassSpec.WARRIOR_ARMS;
                    break;
                case 72:
                    _spec = ClassSpec.WARRIOR_FURY;
                    break;
                case 73:
                    _spec = ClassSpec.WARRIOR_PROTECTION;
                    break;
                case 102:
                    _spec = ClassSpec.DRUID_BALANCE;
                    break;
                case 103:
                    _spec = ClassSpec.DRUID_FERAL;
                    break;
                case 104:
                    _spec = ClassSpec.DRUID_GUARDIAN;
                    break;
                case 105:
                    _spec = ClassSpec.DRUID_RESTORATION;
                    break;
                case 250:
                    _spec = ClassSpec.DEATH_KNIGHT_BLOOD;
                    break;
                case 251:
                    _spec = ClassSpec.DEATH_KNIGHT_FROST;
                    break;
                case 252:
                    _spec = ClassSpec.DEATH_KNIGHT_UNHOLY;
                    break;
                case 253:
                    _spec = ClassSpec.HUNTER_BEASTMASTERY;
                    break;
                case 254:
                    _spec = ClassSpec.HUNTER_MARKSMANSHIP;
                    break;
                case 255:
                    _spec = ClassSpec.HUNTER_SURVIVAL;
                    break;
                case 256:
                    _spec = ClassSpec.PRIEST_DISCIPLINE;
                    break;
                case 257:
                    _spec = ClassSpec.PRIEST_HOLY;
                    break;
                case 258:
                    _spec = ClassSpec.PRIEST_SHADOW;
                    break;
                case 259:
                    _spec = ClassSpec.ROGUE_ASSASSINATION;
                    break;
                case 260:
                    _spec = ClassSpec.ROGUE_OUTLAW;
                    break;
                case 261:
                    _spec = ClassSpec.ROGUE_SUBTLETY;
                    break;
                case 262:
                    _spec = ClassSpec.SHAMAN_ELEMENTAL;
                    break;
                case 263:
                    _spec = ClassSpec.SHAMAN_ENHANCEMENT;
                    break;
                case 264:
                    _spec = ClassSpec.SHAMAN_RESTORATION;
                    break;
                case 265:
                    _spec = ClassSpec.WARLOCK_AFFLICTION;
                    break;
                case 266:
                    _spec = ClassSpec.WARLOCK_DEMONOLOGY;
                    break;
                case 267:
                    _spec = ClassSpec.WARLOCK_DESTRUCTION;
                    break;
                case 268:
                    _spec = ClassSpec.MONK_BREWMASTER;
                    break;
                case 269:
                    _spec = ClassSpec.MONK_WINDWALKER;
                    break;
                case 270:
                    _spec = ClassSpec.MONK_MISTWEAVER;
                    break;
                case 577:
                    _spec = ClassSpec.DEMON_HUNTER_HAVOC;
                    break;
                case 581:
                    _spec = ClassSpec.DEMON_HUNTER_VENGEANCE;
                    break;
            }

            int tal15 = int.Parse(parameters[25].Replace("(", ""));
            int tal25 = int.Parse(parameters[26]);
            int tal30 = int.Parse(parameters[27]);
            int tal35 = int.Parse(parameters[28]);
            int tal40 = int.Parse(parameters[29]);
            int tal45 = int.Parse(parameters[30]);
            int tal50 = int.Parse(parameters[31].Replace(")", ""));
            int pvptal1 = int.Parse(parameters[33]);
            int pvptal2 = int.Parse(parameters[34]);
            int pvptal3 = int.Parse(parameters[35].Replace(")", ""));
            int covId = int.Parse(parameters[37]);
            int soulbindId = int.Parse(parameters[36].Replace("[", ""));

            bool soulbindTreeParsed = false, insertedConduitsParsed = false;
            int parse_detailed_info_start = 39;
            List<int> soulbindTree = new List<int>();
            List<KeyValuePair<int, int>> conduits = new List<KeyValuePair<int, int>>();
            // covenant info: soulbind id, covenant id, selected nodes in soulbind talent tree, active conduits
            for (int i = parse_detailed_info_start; i < parameters.Length && (!soulbindTreeParsed || !insertedConduitsParsed); i++)
            {
                if (!soulbindTreeParsed)
                {
                    soulbindTree.Add(int.Parse(parameters[i].Replace("[", "").Replace("]", "").Replace("(", "").Replace(")", "")));
                    if (parameters[i].Contains("]"))
                        soulbindTreeParsed = true;
                }
                else if (!insertedConduitsParsed)
                {
                    if (parameters[i].Contains("(") && parameters[i + 1].Contains(")"))
                    {
                        conduits.Add(new KeyValuePair<int, int>(int.Parse(parameters[i].Replace("(", "").Replace("[", "")), int.Parse(parameters[i + 1].Replace(")", "").Replace("]", ""))));
                    }
                    if (parameters[i].Contains("]"))
                        insertedConduitsParsed = true;
                }
                else
                    break;
            }

            CombatantCovenantInfo covInfo = new CombatantCovenantInfo();
            covInfo.Covenant = CovenantParser.Parse(covId);
            covInfo.Soulbind = SoubindParser.Parse(soulbindId);
            covInfo.SoulbindTree = soulbindTree;
            covInfo.Conduits = conduits;

            // equipment: item id, enchants, bonuses (only first and last needed?), gems
            List<EquipmentItem> items = new List<EquipmentItem>();
            string reconstructString = String.Join(",", parameters);
            reconstructString = reconstructString.Remove(0, reconstructString.IndexOf(")]],") + 4);
            string itemPattern = @"\([0-9]+,[0-9]+,\([0-9,]*\),\([0-9,]*\),\([0-9,]*\)\)";

            MatchCollection matches = Regex.Matches(reconstructString, itemPattern);

            foreach (Match match in matches)
            {
                string itemIdAndLevelPattern = @"[0-9]+,[0-9]+";
                string[] idIlvl = Regex.Match(match.Value, itemIdAndLevelPattern).Value.Split(',');
                string itemDetailsPattern = @"\([0-9,]*\)";
                MatchCollection itemDetailsMatch = Regex.Matches(match.Value, itemDetailsPattern);
                EquipmentItem item = new EquipmentItem();
                item.ItemId = int.Parse(idIlvl[0]);
                item.ItemLevel = int.Parse(idIlvl[1]);
                string[] enchantsString = itemDetailsMatch[0].Value.Replace("(", "").Replace(")", "").Split(',');
                if (enchantsString.Length > 0 && enchantsString[0] != "")
                    foreach (string ench in enchantsString)
                        item.Enchants.Add(int.Parse(ench));

                string[] bonusesString = itemDetailsMatch[1].Value.Replace("(", "").Replace(")", "").Split(',');
                if (bonusesString.Length > 0 && bonusesString[0] != "")
                    foreach (string bonus in bonusesString)
                        item.Bonuses.Add(int.Parse(bonus));

                string[] gemString = itemDetailsMatch[2].Value.Replace("(", "").Replace(")", "").Split(',');
                if (gemString.Length > 0 && gemString[0] != "")
                    item.Gem = int.Parse(gemString[0]);

                items.Add(item);
            }

            return new CombatantInfo(timeStamp, specId, _spec, guid, tal15, tal25, tal30, tal35, tal40, tal45, tal50, pvptal1, pvptal2, pvptal3, covInfo, items, team_number);
        }

        private UnitDied ParseUnitDied(CombatLogEntry entry, string[] parameters)
        {
            string unknown = parameters[0];
            return new UnitDied(entry, unknown);
        }


        private LogSpell ParseLogSpell(string[] parameters)
        {
            int spellId = int.Parse(parameters[0]);

            if (Spells.ContainsKey(spellId))
            {
                return Spells[spellId];
            }
            else
            {
                LogSpell spell = LogSpell.ParseSpell(parameters);
                Spells.Add(spell.SpellId, spell);
                return spell;
            }
        }

        private LogSubject ParseLogSubject(string[] parameters)
        {
            string guid = parameters[0];
            
            if (Subjects.ContainsKey(guid))
            {
                return Subjects[guid];
            }
            else
            {
                LogSubject subject = LogSubject.ParseSubject(parameters);
                Subjects.Add(subject.GUID, subject);
                return subject;
            }
        }
    }

    internal class ParseFailedException : Exception {
        public ParseFailedException(string msg) : base("Parsing: " + msg) { }
    }
}
