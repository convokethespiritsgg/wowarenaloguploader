﻿using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;

namespace CombatLogParser
{
    public class SimpleCombatantInfo
    {
        public string GUID { get; set; }
        public string PlayerName { get; set; }
        public int TeamNumber { get; set; }
        public ClassSpec Spec { get; set; }
        public SimpleCombatantInfo() { }
    }

    public class ReportKey : IComparable
    {
        public DateTime Time { get; set; }
        public string Arena { get; set; }

        public string PrefixForFile;

        public string ArenaFormat { get; set; }
        public List<SimpleCombatantInfo> Fighters { get; set; }
        public ArenaMatchEnd Result { get; set; }
        public int ZoneId { get; set; }
        public int Version { get; set; }

        public HashSet<string> Issuers { get; set; } // Player(s), who has sent the report
        public ReportKey()
        {
            Fighters = new List<SimpleCombatantInfo>();
            Version = Parser.Version;
        }

        public ReportKey(DateTime date, string arena, int zone_id, string arena_format, List<BaseLogEntry> combatant_info, ArenaMatchEnd endResult, string issuer_name)
        {
            Time = date;
            Arena = arena;
            ArenaFormat = arena_format;
            Fighters = new List<SimpleCombatantInfo>();
            ZoneId = zone_id;
            Issuers = new HashSet<string>();

            if (issuer_name != null)
                Issuers.Add(issuer_name);

            foreach (BaseLogEntry el in combatant_info)
            {
                if (el.Type == LogEntryType.COMBATANT_INFO)
                {
                    CombatantInfo ci = (CombatantInfo)el;
                    Fighters.Add(new SimpleCombatantInfo { GUID = ci.GUID, PlayerName = ci.PlayerName, TeamNumber = ci.TeamNumber, Spec = ci.Spec });
                }
            }

            PrefixForFile = zone_id switch
            {
                1552 => "AshamanesFall",
                1504 => "BlackRookHold",
                1672 => "BladesEdge",
                617  => "Dalaran",
                1825 => "KulTiras",
                9992 => "Mugambala",
                1505 => "Nagrand",
                572  => "RuinsOfLordaeron",
                2167 => "TheRobodrome",
                1134 => "TheTigersPeak",
                980  => "Tolviron",
                2373 => "EmpyreanDomain",
                _    => "UnknownArena",
            };

            Result = endResult;
            Version = Parser.Version;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is ReportKey otherObject)
            {
                if (Time.CompareTo(otherObject.Time) == 0 && Arena.CompareTo(otherObject.Arena) == 0 && ArenaFormat.CompareTo(otherObject.ArenaFormat) == 0)
                    return 0;
                else
                    return 1;
            }
            else
                throw new ArgumentException("Object is not a CombatReport");
        }

        public override string ToString()
        {
            return GetType().Name + " (" + Time + " " + Arena + " " + ArenaFormat + ")";
        }

    }
}
