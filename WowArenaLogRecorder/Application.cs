﻿using System;
using System.Reflection;
using System.Windows.Forms;
using WoWArenaLogUploader.Modules;
using WoWArenaLogUploader.UI;

namespace WoWArenaLogUploader
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SettingsManager manager = SettingsManager.Get();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (manager.StartupIsOk().Item1 == true)
            {
                Application.Run(new MainForm());
            }
            else
            {
                Application.Run(new GreetingsForm());
            }
        }
    }
}
