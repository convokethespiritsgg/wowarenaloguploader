﻿using CombatLogParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WoWArenaLogUploader.Modules;
using AutoUpdaterDotNET;
using System.Text.Json;
using System.Threading;

namespace WoWArenaLogUploader.UI
{
    public partial class MainForm : Form
    {
        private bool IsInAllReportPosting = false;
#if DEBUG
        private readonly string SERVER_URL = "http://localhost:4000/api";
        private readonly string LINK_URL = "https://localhost:44330";
#else
        private readonly string SERVER_URL = "https://convokethespirits.gg/api";
        private readonly string LINK_URL = "https://convokethespirits.gg";
        private readonly string VERSION_CHECK_URL = "https://convokethespirits.gg/files/appversion.xml";
#endif
        private readonly SettingsManager settingsManager = SettingsManager.Get();
        private readonly LogRecorder reportManager;
        private readonly Logger logger = Logger.Get();
        private static readonly HttpClient client = new HttpClient();


        private readonly BindingList<ReportKey> capturedReports = new BindingList<ReportKey>();
        private Dictionary<ReportKey, string> reportLinks = new Dictionary<ReportKey, string>();

        private string logFilePath;

        private bool isLiveCapture = true;

        private ContextMenuStrip trayMenu;

        public MainForm()
        {
            InitializeComponent();

            InitializeTray();

            InitializeSettings();

            reportsGridView.DataSource = capturedReports;
            reportsGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            reportsGridView.Columns[3].Visible = false;
            reportsGridView.Columns[4].Visible = false;
            reportsGridView.Columns[5].Visible = false;
            reportsGridView.Columns[6].Visible = false;

            closeLogFile.Enabled = false;

#if RELEASE
            this.devPage.Dispose();
#endif

            logFilePath = settingsManager.GetCombatLogLocation();

            this.Text = Assembly.GetExecutingAssembly().GetName().Name + " " + Assembly.GetExecutingAssembly().GetName().Version;
            connectionCheckLabel.Text = "";

            Task.Factory.StartNew(() => CheckAppToken(settingsManager.GetApplicationToken()));

            reportManager = new LogRecorder(OnReportFind);
#if RELEASE
            AutoUpdater.Mandatory = true;
            AutoUpdater.UpdateMode = Mode.ForcedDownload;
            AutoUpdater.RunUpdateAsAdmin = false;
            AutoUpdater.Start(VERSION_CHECK_URL);
#endif
        }

        private void InitializeSettings()
        {
            string gameLocation = settingsManager.GetGameLocation();
            if (gameLocation != null && gameLocation.Length > 0)
                gameLocationTextBox.Text = gameLocation;

            appTokenTextBox.Text = settingsManager.GetApplicationToken();

            autoOpenReportCheckbox.Checked = settingsManager.GetAutoOpenReport();
            autoPostReportCheckbox.Checked = settingsManager.GetLiveAutoPosting();
        }

        private void InitializeTray()
        {
            trayIcon.Visible = true;
            trayMenu = new ContextMenuStrip();

            ToolStripMenuItem showWindow = new ToolStripMenuItem
            {
                MergeIndex = 0,
                Text = "Show"
            };
            showWindow.Click += ShowWindow_Click;

            ToolStripMenuItem exitWindow = new ToolStripMenuItem
            {
                MergeIndex = 1,
                Text = "Exit"
            };
            exitWindow.Click += ExitWindow_Click;

            trayMenu.Items.AddRange(new ToolStripMenuItem[] { showWindow, exitWindow });
            trayIcon.ContextMenuStrip = trayMenu;

            trayIcon.BalloonTipTitle = "Report for arena match ready!";
            trayIcon.BalloonTipIcon = ToolTipIcon.Info;
        }

        private void ChangeLogLocation_Click(object sender, EventArgs e)
        {
            using FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                settingsManager.SetGameLocation(folderBrowserDialog.SelectedPath);
                gameLocationTextBox.Text = folderBrowserDialog.SelectedPath;
            }

        }

        private void OpenLogFile_Click(object sender, EventArgs e)
        {
            using FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                logFilePath = folderBrowserDialog.SelectedPath;
                openedLogFileTextBox.Text = logFilePath;
            }
        }

        private void CancelScanLogFile_Click(object sender, EventArgs e)
        {
            reportManager.RequestStop();
        }

        private delegate void UpdateUI();

        private void ScanLogFile_Click(object sender, EventArgs e)
        {
            string locale = "enUS";
            if (isLiveCapture)
            {
                string localePath = settingsManager.GetGameLocation() + "\\_retail_\\WTF\\Config.wtf";
                using (StreamReader sr = new StreamReader(localePath))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        if (line.Contains("textLocale"))
                        {
                            locale = line.Split(' ')[2].Replace("\"", "").Trim();
                            break;
                        }
                    }
                }
            }
            else
            {
                locale = "ruRU";
            }

            logger.WriteLine(GetType().Name, "found locale " + locale);

            capturedReports.Clear();
            scanLogFile.Enabled = false;
            closeLogFile.Enabled = true;


            logger.WriteLine(GetType().Name, "scanning " + logFilePath + ", live " + isLiveCapture);

            reportManager.SetLocale(locale);

            Task.Factory.StartNew(() => reportManager.ScanFileForArenaMatches(logFilePath, isLiveCapture, OnFinish));
        }

        public delegate void OnReportFindDelegate(ReportKey report, ReportData data);
        public delegate void OnFinishDelegate();

        void OnReportFind(ReportKey report, ReportData data)
        {
            if (settingsManager.GetLiveAutoPosting() && isLiveCapture)
            {
                reportsGridView.Invoke(new UpdateUI(() =>
                {
                    capturedReports.Insert(0, report);
                }));

                Task.Factory.StartNew(async delegate
                {
                    await Task.Delay(1000);
                    return PostReport(new ReportObject(report, data));
                });
            }
            else
            {
                reportsGridView.Invoke(new UpdateUI(() =>
                {
                    capturedReports.Insert(0, report);
                }));
            }

            TellTotalReportCount();
        }

        void OnFinish()
        {
            scanLogFile.Invoke(new UpdateUI(() =>
            {
                scanLogFile.Enabled = true;
                if (!isLiveCapture)
                {
                    closeLogFile.Enabled = false;
                }
            })
            );
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void TrayIcon_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void ShowWindow_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void ExitWindow_Click(object sender, EventArgs e)
        {
            Close();
        }

        async Task PostReport(ReportObject report)
        {
            DataGridViewRow report_row = null;

            foreach (var row in reportsGridView.Rows)
            {
                var row_val = (DataGridViewRow)row;
                var rep_key = (ReportKey)row_val.DataBoundItem;
                if (rep_key == report.ArenaParams)
                {
                    report_row = row_val;
                }
            }

            report_row.DefaultCellStyle.BackColor = Color.LemonChiffon;

            try
            {
                string appToken = settingsManager.GetApplicationToken();
                if (appToken.Length == 0)
                    appToken = "public";

                string responseString = "";
                // === PrePost ===
                string sendData = "{\"AppToken\":\"" + appToken + "\", \"ReportString\":\"" + Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(report.ArenaParams))) + "\"}";
                
                var httpContent = new StringContent(sendData.ToString(), Encoding.UTF8, "application/json");
                
                var response = await client.PostAsync($"{SERVER_URL}/report/prepost", httpContent);

                if (response.IsSuccessStatusCode)
                {
                    responseString = await response.Content.ReadAsStringAsync();
                    if (responseString == "")
                    {
                        // We don't find report on server, so we post one.
                        // === Post ===

                        sendData = "{\"AppToken\":\"" + appToken + "\", \"ReportString\":\"" + Convert.ToBase64String(Encoding.UTF8.GetBytes(report.Serialize())) + "\"}";

                        httpContent = new StringContent(sendData.ToString(), Encoding.UTF8, "application/json");

                        response = await client.PostAsync($"{SERVER_URL}/report/post", httpContent);

                        if (response.IsSuccessStatusCode)
                        {
                            responseString = await response.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                responseString = await response.Content.ReadAsStringAsync();
                            }
                            report_row.Cells[1].ToolTipText = $"Server response is unsuccessful ({response.StatusCode}) Message: {responseString}";
                            report_row.DefaultCellStyle.BackColor = Color.Orange;
                            return;
                        }
                    }
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        responseString = await response.Content.ReadAsStringAsync();
                    }
                    
                    report_row.Cells[1].ToolTipText = $"Server response is unsuccessful ({response.StatusCode}) Message: {responseString}";
                    report_row.DefaultCellStyle.BackColor = Color.Orange;
                    return;
                }

                report_row.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                reportLinks[report.ArenaParams] = $"{LINK_URL}/report/{responseString}";

                // ===========================================
                reportLinkTextBox.Invoke(new UpdateUI(() =>
                {
                    ChangeReportLink(report.ArenaParams);
                }));
                // ===========================================
                if (autoOpenReportCheckbox.Checked && !IsInAllReportPosting)
                {
                    Process process = new Process();
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.FileName = reportLinks[report.ArenaParams];
                    process.Start();
                }
                // ===========================================

                logger.WriteLine(GetType().Name, responseString);
            }
            catch (UriFormatException e)
            {
                logger.WriteLine(GetType().Name, e.ToString());
                report_row.DefaultCellStyle.BackColor = Color.Orange;
            }
        }

        private void ReportsGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReportKey rep = (ReportKey)reportsGridView.CurrentRow.DataBoundItem;
            logger.WriteLine(GetType().Name, "selected " + rep.Time + " " + rep.Arena);
            if (!reportLinks.ContainsKey(rep))
            {
                ReportData rep_data = reportManager.GetLogForReport(rep);
                ReportObject report = new ReportObject(rep, rep_data);

#if DEBUG
                string reportsPath = settingsManager.GetReportsLocation();
                Directory.CreateDirectory(reportsPath);

                using var stream = File.CreateText(reportsPath + "\\" + rep.Arena.Replace("\"", "") + ".json");
                stream.WriteLine(report.Serialize());
#endif

                Task.Factory.StartNew(() => PostReport(report));
            }
        }

        async Task CheckAppToken(string appToken)
        {
            try
            {
                if (appToken.Length == 0)
                {
                    reportLinkTextBox.Invoke(new UpdateUI(() =>
                    {
                        connectionCheckLabel.Text = "AppToken is empty. Consider that posted reports are public.";
                        connectionCheckLabel.ForeColor = Color.DarkRed;
                    }));

                    return;
                }
                else
                {
                    reportLinkTextBox.Invoke(new UpdateUI(() =>
                    {
                        connectionCheckLabel.Text = "Checking...";
                        connectionCheckLabel.ForeColor = Color.DarkGoldenrod;
                    }));
                }

                string sendData = "{\"AppToken\":\"" + appToken + "\"}";
                var httpContent = new StringContent(sendData.ToString(), Encoding.UTF8, "application/json");

                logger.WriteLine(GetType().Name, sendData.ToString());

                var response = await client.PostAsync($"{SERVER_URL}/apptokentest/test", httpContent);

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    reportLinkTextBox.Invoke(new UpdateUI(() =>
                    {
                        connectionCheckLabel.Text = "Server response: Token is incorrect";
                        connectionCheckLabel.ForeColor = Color.Crimson;
                    }));
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    reportLinkTextBox.Invoke(new UpdateUI(() =>
                    {
                        connectionCheckLabel.Text = "Server response: Ok";
                        connectionCheckLabel.ForeColor = Color.Green;
                    }));
                }
                else
                {
                    reportLinkTextBox.Invoke(new UpdateUI(() =>
                    {
                        connectionCheckLabel.Text = $"Network error: {response.StatusCode}";
                        connectionCheckLabel.ForeColor = Color.DarkRed;
                    }));
                }
            }
            catch (Exception e)
            {
                logger.WriteLine(GetType().Name, e.ToString());

                reportLinkTextBox.Invoke(new UpdateUI(() =>
                {
                    connectionCheckLabel.Text = "Network error";
                    connectionCheckLabel.ForeColor = Color.DarkRed;
                }));
            }
        }

        private void appTokenSave_Click(object sender, EventArgs e)
        {

            Task.Factory.StartNew(() => CheckAppToken(appTokenTextBox.Text));

            settingsManager.SetApplicationToken(appTokenTextBox.Text);
        }

        private void liveScanButton_CheckedChanged(object sender, EventArgs e)
        {
            isLiveCapture = true;
        }

        private void fileScanButton_CheckedChanged(object sender, EventArgs e)
        {
            isLiveCapture = false;
        }

        private void ChangeReportLink(ReportKey rep)
        {
            if (reportLinks.ContainsKey(rep))
            {
                reportLinkTextBox.Text = reportLinks[rep];
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"ChangeReportLink: report don't present: {rep.ToString()}");
                reportLinkTextBox.Text = "";
            }
        }

        private void reportsGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ReportKey rep = (ReportKey)reportsGridView.CurrentRow.DataBoundItem;
            logger.WriteLine(GetType().Name, "selected " + rep.Time + " " + rep.Arena);

            reportLinkTextBox.Invoke(new UpdateUI(() =>
            {
                ChangeReportLink(rep);
            }));
        }

        private void reportLinkTextBox_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            //Clipboard.SetText(reportLinkTextBox.Text);
            if (reportLinkTextBox.Text.Length > 0 && reportLinkTextBox.Text != "none")
            {
                Process process = new Process();
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.FileName = reportLinkTextBox.Text;

                process.Start();
            }
        }

        private void autoOpenReportCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            settingsManager.SetAutoOpenReport(autoOpenReportCheckbox.Checked);
        }

        private void autoPostReport_CheckedChanged(object sender, EventArgs e)
        {
            settingsManager.SetLiveAutoPosting(autoPostReportCheckbox.Checked);
        }

        private void MainTabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (MainTabControl.SelectedIndex != 0)
            {
                ExplanationLabel.Visible = false;
                reportLinkLabel.Visible = false;
                reportLinkTextBox.Visible = false;
                PostAllButton.Visible = false;
                PostAllReportProgressBar.Visible = false;
            }
            else
            {
                ExplanationLabel.Visible = true;
                reportLinkLabel.Visible = true;
                reportLinkTextBox.Visible = true;
                PostAllButton.Visible = true;
                PostAllReportProgressBar.Visible = true;
            }
        }

        async Task PostAllReports()
        {

            PostAllReportProgressBar.Invoke(new UpdateUI(() =>
            {
                PostAllReportProgressBar.Visible = true;
            }));
            
            int current = 0;
            int total = capturedReports.Count;

            foreach (var rep_key in capturedReports)
            {
                if (reportLinks.ContainsKey(rep_key))
                {
                    current++;
                    continue;
                }

                ReportData rep_data = reportManager.GetLogForReport(rep_key);
                ReportObject report = new ReportObject(rep_key, rep_data);
                await PostReport(report).ContinueWith(
                    (task) => {
                        current++;
                        PostAll_UI_UpdateProgress(current, total);
                    });
                if (!IsInAllReportPosting)
                {
                    break;
                }
            }

            PostAllReportProgressBar.Invoke(new UpdateUI(() =>
            {
                PostAllReportProgressBar.Visible = false;
            }));
            TellTotalReportCount();

            IsInAllReportPosting = false;
            PostAll_UI_UpdateProgressButton();
        }

        private void PostAllButton_Click(object sender, EventArgs e)
        {
            if (!IsInAllReportPosting)
            {
                IsInAllReportPosting = true;
                Task.Factory.StartNew(() => PostAllReports());
            }
            else
            {
                IsInAllReportPosting = false;
            }
            PostAll_UI_UpdateProgressButton();
        }

        public void PostAll_UI_UpdateProgressButton()
        {
            PostAllButton.Invoke(new UpdateUI(() =>
            {
                if (IsInAllReportPosting)
                {
                    PostAllButton.Text = "Cancel posting";
                }
                else
                {
                    PostAllButton.Text = "Post all reports";
                }
            }));
        }

        public void PostAll_UI_UpdateProgress(int posted, int total)
        {
            allReportPostingStatus.Invoke(new UpdateUI(() =>
            {
                allReportPostingStatus.Text = $"Posted {posted} / {total}";
                PostAllReportProgressBar.Value = (int)((float)posted * 100) / total;
            }));

        }

        public void TellTotalReportCount()
        {
            allReportPostingStatus.Invoke(new UpdateUI(() =>
            {
                allReportPostingStatus.Text = $"{capturedReports.Count} reports";
            }));
        }
    }
}
