﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using WoWArenaLogUploader.Modules;

namespace WoWArenaLogUploader.UI
{
    public partial class GreetingsForm : Form
    {
        private readonly SettingsManager settingsManager = SettingsManager.Get();

        public GreetingsForm()
        {
            InitializeComponent();

            wowDirectoryTextBox.Text = settingsManager.GetGameLocation();

            this.Text = Assembly.GetExecutingAssembly().GetName().Name + " " + Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void wowDirectoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                settingsManager.SetGameLocation(dialog.SelectedPath);
                wowDirectoryTextBox.Text = dialog.SelectedPath;
            }

            var (is_ok, err_msg) = settingsManager.StartupIsOk();

            if (is_ok)
            {
                continueButton.Enabled = true;
            }

            statusLabel.Text = err_msg;

        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            var mainform = new MainForm();
            mainform.Closed += (s, args) => this.Close();
            mainform.Show();
        }
    }
}
