﻿namespace WoWArenaLogUploader.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.fromLog = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.liveScanButton = new System.Windows.Forms.RadioButton();
            this.fileScanButton = new System.Windows.Forms.RadioButton();
            this.reportsGridView = new System.Windows.Forms.DataGridView();
            this.closeLogFile = new System.Windows.Forms.Button();
            this.scanLogFile = new System.Windows.Forms.Button();
            this.optionsPage = new System.Windows.Forms.TabPage();
            this.accountSettings = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.connectionCheckLabel = new System.Windows.Forms.Label();
            this.appTokenTextBox = new System.Windows.Forms.TextBox();
            this.appTokenSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.fileOptions = new System.Windows.Forms.GroupBox();
            this.otherOptions = new System.Windows.Forms.GroupBox();
            this.autoPostReportCheckbox = new System.Windows.Forms.CheckBox();
            this.autoOpenReportCheckbox = new System.Windows.Forms.CheckBox();
            this.gameLocationTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.changeLogLocation = new System.Windows.Forms.Button();
            this.aboutPage = new System.Windows.Forms.TabPage();
            this.AboutTextBox = new System.Windows.Forms.TextBox();
            this.devPage = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.openLogFile = new System.Windows.Forms.Button();
            this.openedLogFileTextBox = new System.Windows.Forms.TextBox();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.reportLinkLabel = new System.Windows.Forms.Label();
            this.reportLinkTextBox = new System.Windows.Forms.LinkLabel();
            this.ExplanationLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PostAllButton = new System.Windows.Forms.Button();
            this.PostAllReportProgressBar = new System.Windows.Forms.ProgressBar();
            this.allReportPostingStatus = new System.Windows.Forms.Label();
            this.MainTabControl.SuspendLayout();
            this.fromLog.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsGridView)).BeginInit();
            this.optionsPage.SuspendLayout();
            this.accountSettings.SuspendLayout();
            this.fileOptions.SuspendLayout();
            this.otherOptions.SuspendLayout();
            this.aboutPage.SuspendLayout();
            this.devPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.fromLog);
            this.MainTabControl.Controls.Add(this.optionsPage);
            this.MainTabControl.Controls.Add(this.aboutPage);
            this.MainTabControl.Controls.Add(this.devPage);
            this.MainTabControl.Location = new System.Drawing.Point(12, 12);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(760, 617);
            this.MainTabControl.TabIndex = 0;
            this.MainTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.MainTabControl_Selected);
            // 
            // fromLog
            // 
            this.fromLog.BackColor = System.Drawing.Color.Transparent;
            this.fromLog.Controls.Add(this.panel1);
            this.fromLog.Controls.Add(this.reportsGridView);
            this.fromLog.Controls.Add(this.closeLogFile);
            this.fromLog.Controls.Add(this.scanLogFile);
            this.fromLog.Location = new System.Drawing.Point(4, 25);
            this.fromLog.Name = "fromLog";
            this.fromLog.Padding = new System.Windows.Forms.Padding(3);
            this.fromLog.Size = new System.Drawing.Size(752, 588);
            this.fromLog.TabIndex = 0;
            this.fromLog.Text = "Capture log";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.liveScanButton);
            this.panel1.Controls.Add(this.fileScanButton);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(327, 70);
            this.panel1.TabIndex = 10;
            // 
            // liveScanButton
            // 
            this.liveScanButton.AutoSize = true;
            this.liveScanButton.Checked = true;
            this.liveScanButton.Location = new System.Drawing.Point(10, 11);
            this.liveScanButton.Name = "liveScanButton";
            this.liveScanButton.Size = new System.Drawing.Size(280, 20);
            this.liveScanButton.TabIndex = 8;
            this.liveScanButton.TabStop = true;
            this.liveScanButton.Text = "Live scan (Detect only new arena matches)";
            this.liveScanButton.UseVisualStyleBackColor = true;
            this.liveScanButton.CheckedChanged += new System.EventHandler(this.liveScanButton_CheckedChanged);
            // 
            // fileScanButton
            // 
            this.fileScanButton.AutoSize = true;
            this.fileScanButton.Location = new System.Drawing.Point(10, 36);
            this.fileScanButton.Name = "fileScanButton";
            this.fileScanButton.Size = new System.Drawing.Size(312, 20);
            this.fileScanButton.TabIndex = 9;
            this.fileScanButton.Text = "Full scan (Track all matches from combatlog file)";
            this.fileScanButton.UseVisualStyleBackColor = true;
            this.fileScanButton.CheckedChanged += new System.EventHandler(this.fileScanButton_CheckedChanged);
            // 
            // reportsGridView
            // 
            this.reportsGridView.AllowUserToAddRows = false;
            this.reportsGridView.AllowUserToDeleteRows = false;
            this.reportsGridView.AllowUserToResizeRows = false;
            this.reportsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.reportsGridView.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.reportsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportsGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.reportsGridView.Location = new System.Drawing.Point(3, 89);
            this.reportsGridView.Name = "reportsGridView";
            this.reportsGridView.RowHeadersVisible = false;
            this.reportsGridView.RowHeadersWidth = 62;
            this.reportsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.reportsGridView.Size = new System.Drawing.Size(746, 497);
            this.reportsGridView.TabIndex = 7;
            this.reportsGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ReportsGridView_CellDoubleClick);
            this.reportsGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.reportsGridView_CellMouseClick);
            // 
            // closeLogFile
            // 
            this.closeLogFile.Location = new System.Drawing.Point(346, 54);
            this.closeLogFile.Name = "closeLogFile";
            this.closeLogFile.Size = new System.Drawing.Size(89, 29);
            this.closeLogFile.TabIndex = 6;
            this.closeLogFile.Text = "Cancel";
            this.closeLogFile.UseVisualStyleBackColor = true;
            this.closeLogFile.Click += new System.EventHandler(this.CancelScanLogFile_Click);
            // 
            // scanLogFile
            // 
            this.scanLogFile.Location = new System.Drawing.Point(346, 13);
            this.scanLogFile.Name = "scanLogFile";
            this.scanLogFile.Size = new System.Drawing.Size(89, 29);
            this.scanLogFile.TabIndex = 5;
            this.scanLogFile.Text = "Scan";
            this.scanLogFile.UseVisualStyleBackColor = true;
            this.scanLogFile.Click += new System.EventHandler(this.ScanLogFile_Click);
            // 
            // optionsPage
            // 
            this.optionsPage.BackColor = System.Drawing.Color.White;
            this.optionsPage.Controls.Add(this.accountSettings);
            this.optionsPage.Controls.Add(this.fileOptions);
            this.optionsPage.Location = new System.Drawing.Point(4, 25);
            this.optionsPage.Name = "optionsPage";
            this.optionsPage.Padding = new System.Windows.Forms.Padding(3);
            this.optionsPage.Size = new System.Drawing.Size(752, 588);
            this.optionsPage.TabIndex = 1;
            this.optionsPage.Text = "Options";
            // 
            // accountSettings
            // 
            this.accountSettings.Controls.Add(this.label5);
            this.accountSettings.Controls.Add(this.connectionCheckLabel);
            this.accountSettings.Controls.Add(this.appTokenTextBox);
            this.accountSettings.Controls.Add(this.appTokenSave);
            this.accountSettings.Controls.Add(this.label3);
            this.accountSettings.Location = new System.Drawing.Point(12, 217);
            this.accountSettings.Name = "accountSettings";
            this.accountSettings.Size = new System.Drawing.Size(725, 99);
            this.accountSettings.TabIndex = 18;
            this.accountSettings.TabStop = false;
            this.accountSettings.Text = "Account Settings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 18;
            this.label5.Text = "Status:";
            // 
            // connectionCheckLabel
            // 
            this.connectionCheckLabel.AutoSize = true;
            this.connectionCheckLabel.Location = new System.Drawing.Point(60, 81);
            this.connectionCheckLabel.Name = "connectionCheckLabel";
            this.connectionCheckLabel.Size = new System.Drawing.Size(166, 16);
            this.connectionCheckLabel.TabIndex = 17;
            this.connectionCheckLabel.Text = "connection to server status";
            // 
            // appTokenTextBox
            // 
            this.appTokenTextBox.Location = new System.Drawing.Point(6, 47);
            this.appTokenTextBox.Name = "appTokenTextBox";
            this.appTokenTextBox.Size = new System.Drawing.Size(593, 22);
            this.appTokenTextBox.TabIndex = 15;
            // 
            // appTokenSave
            // 
            this.appTokenSave.Location = new System.Drawing.Point(605, 42);
            this.appTokenSave.Name = "appTokenSave";
            this.appTokenSave.Size = new System.Drawing.Size(106, 31);
            this.appTokenSave.TabIndex = 16;
            this.appTokenSave.Text = "Save";
            this.appTokenSave.UseVisualStyleBackColor = true;
            this.appTokenSave.Click += new System.EventHandler(this.appTokenSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "AppToken:";
            // 
            // fileOptions
            // 
            this.fileOptions.Controls.Add(this.otherOptions);
            this.fileOptions.Controls.Add(this.gameLocationTextBox);
            this.fileOptions.Controls.Add(this.label2);
            this.fileOptions.Controls.Add(this.changeLogLocation);
            this.fileOptions.Location = new System.Drawing.Point(6, 6);
            this.fileOptions.Name = "fileOptions";
            this.fileOptions.Size = new System.Drawing.Size(737, 494);
            this.fileOptions.TabIndex = 0;
            this.fileOptions.TabStop = false;
            this.fileOptions.Text = "File Options";
            // 
            // otherOptions
            // 
            this.otherOptions.Controls.Add(this.autoPostReportCheckbox);
            this.otherOptions.Controls.Add(this.autoOpenReportCheckbox);
            this.otherOptions.Location = new System.Drawing.Point(6, 95);
            this.otherOptions.Name = "otherOptions";
            this.otherOptions.Size = new System.Drawing.Size(725, 110);
            this.otherOptions.TabIndex = 5;
            this.otherOptions.TabStop = false;
            this.otherOptions.Text = "Other Options";
            // 
            // autoPostReportCheckbox
            // 
            this.autoPostReportCheckbox.AutoSize = true;
            this.autoPostReportCheckbox.Location = new System.Drawing.Point(10, 47);
            this.autoPostReportCheckbox.Name = "autoPostReportCheckbox";
            this.autoPostReportCheckbox.Size = new System.Drawing.Size(289, 20);
            this.autoPostReportCheckbox.TabIndex = 12;
            this.autoPostReportCheckbox.Text = "Automatically post report when live scanning";
            this.autoPostReportCheckbox.UseVisualStyleBackColor = true;
            this.autoPostReportCheckbox.CheckedChanged += new System.EventHandler(this.autoPostReport_CheckedChanged);
            // 
            // autoOpenReportCheckbox
            // 
            this.autoOpenReportCheckbox.AutoSize = true;
            this.autoOpenReportCheckbox.Location = new System.Drawing.Point(10, 22);
            this.autoOpenReportCheckbox.Name = "autoOpenReportCheckbox";
            this.autoOpenReportCheckbox.Size = new System.Drawing.Size(317, 20);
            this.autoOpenReportCheckbox.TabIndex = 11;
            this.autoOpenReportCheckbox.Text = "Automatically open report when manually posting";
            this.autoOpenReportCheckbox.UseVisualStyleBackColor = true;
            this.autoOpenReportCheckbox.CheckedChanged += new System.EventHandler(this.autoOpenReportCheckbox_CheckedChanged);
            // 
            // gameLocationTextBox
            // 
            this.gameLocationTextBox.Location = new System.Drawing.Point(10, 45);
            this.gameLocationTextBox.Name = "gameLocationTextBox";
            this.gameLocationTextBox.ReadOnly = true;
            this.gameLocationTextBox.Size = new System.Drawing.Size(675, 22);
            this.gameLocationTextBox.TabIndex = 1;
            this.gameLocationTextBox.Text = "<Not set>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Path to World of Warcraft directory:";
            // 
            // changeLogLocation
            // 
            this.changeLogLocation.Location = new System.Drawing.Point(691, 45);
            this.changeLogLocation.Name = "changeLogLocation";
            this.changeLogLocation.Size = new System.Drawing.Size(34, 23);
            this.changeLogLocation.TabIndex = 3;
            this.changeLogLocation.Text = "...";
            this.changeLogLocation.UseVisualStyleBackColor = true;
            this.changeLogLocation.Click += new System.EventHandler(this.ChangeLogLocation_Click);
            // 
            // aboutPage
            // 
            this.aboutPage.Controls.Add(this.AboutTextBox);
            this.aboutPage.Location = new System.Drawing.Point(4, 25);
            this.aboutPage.Name = "aboutPage";
            this.aboutPage.Size = new System.Drawing.Size(752, 588);
            this.aboutPage.TabIndex = 2;
            this.aboutPage.Text = "About";
            this.aboutPage.UseVisualStyleBackColor = true;
            // 
            // AboutTextBox
            // 
            this.AboutTextBox.Location = new System.Drawing.Point(3, 3);
            this.AboutTextBox.Multiline = true;
            this.AboutTextBox.Name = "AboutTextBox";
            this.AboutTextBox.ReadOnly = true;
            this.AboutTextBox.Size = new System.Drawing.Size(746, 583);
            this.AboutTextBox.TabIndex = 0;
            this.AboutTextBox.Text = resources.GetString("AboutTextBox.Text");
            // 
            // devPage
            // 
            this.devPage.Controls.Add(this.label1);
            this.devPage.Controls.Add(this.openLogFile);
            this.devPage.Controls.Add(this.openedLogFileTextBox);
            this.devPage.Location = new System.Drawing.Point(4, 25);
            this.devPage.Name = "devPage";
            this.devPage.Padding = new System.Windows.Forms.Padding(3);
            this.devPage.Size = new System.Drawing.Size(752, 588);
            this.devPage.TabIndex = 3;
            this.devPage.Text = "Developer";
            this.devPage.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Select file:";
            // 
            // openLogFile
            // 
            this.openLogFile.Location = new System.Drawing.Point(618, 28);
            this.openLogFile.Name = "openLogFile";
            this.openLogFile.Size = new System.Drawing.Size(45, 31);
            this.openLogFile.TabIndex = 16;
            this.openLogFile.Text = "...";
            this.openLogFile.UseVisualStyleBackColor = true;
            this.openLogFile.Click += new System.EventHandler(this.OpenLogFile_Click);
            // 
            // openedLogFileTextBox
            // 
            this.openedLogFileTextBox.Location = new System.Drawing.Point(19, 33);
            this.openedLogFileTextBox.Name = "openedLogFileTextBox";
            this.openedLogFileTextBox.ReadOnly = true;
            this.openedLogFileTextBox.Size = new System.Drawing.Size(593, 22);
            this.openedLogFileTextBox.TabIndex = 15;
            this.openedLogFileTextBox.Text = "<None>";
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "WoWArenaLogVisualizer";
            this.trayIcon.Visible = true;
            this.trayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseClick);
            this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // reportLinkLabel
            // 
            this.reportLinkLabel.AutoSize = true;
            this.reportLinkLabel.Location = new System.Drawing.Point(16, 681);
            this.reportLinkLabel.Name = "reportLinkLabel";
            this.reportLinkLabel.Size = new System.Drawing.Size(170, 16);
            this.reportLinkLabel.TabIndex = 1;
            this.reportLinkLabel.Text = "Link to report. Click to open:";
            // 
            // reportLinkTextBox
            // 
            this.reportLinkTextBox.AutoSize = true;
            this.reportLinkTextBox.Location = new System.Drawing.Point(179, 681);
            this.reportLinkTextBox.Name = "reportLinkTextBox";
            this.reportLinkTextBox.Size = new System.Drawing.Size(38, 16);
            this.reportLinkTextBox.TabIndex = 2;
            this.reportLinkTextBox.TabStop = true;
            this.reportLinkTextBox.Text = "none";
            this.reportLinkTextBox.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.reportLinkTextBox_LinkClicked);
            // 
            // ExplanationLabel
            // 
            this.ExplanationLabel.AutoSize = true;
            this.ExplanationLabel.Location = new System.Drawing.Point(16, 632);
            this.ExplanationLabel.Name = "ExplanationLabel";
            this.ExplanationLabel.Size = new System.Drawing.Size(300, 16);
            this.ExplanationLabel.TabIndex = 3;
            this.ExplanationLabel.Text = "Note: double-click on report to post it to the server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 15);
            this.label4.TabIndex = 18;
            this.label4.Text = "label4";
            // 
            // PostAllButton
            // 
            this.PostAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PostAllButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PostAllButton.Location = new System.Drawing.Point(632, 629);
            this.PostAllButton.Name = "PostAllButton";
            this.PostAllButton.Size = new System.Drawing.Size(140, 29);
            this.PostAllButton.TabIndex = 4;
            this.PostAllButton.Text = "Post all reports";
            this.PostAllButton.UseVisualStyleBackColor = true;
            this.PostAllButton.Click += new System.EventHandler(this.PostAllButton_Click);
            // 
            // PostAllReportProgressBar
            // 
            this.PostAllReportProgressBar.Location = new System.Drawing.Point(446, 629);
            this.PostAllReportProgressBar.Name = "PostAllReportProgressBar";
            this.PostAllReportProgressBar.Size = new System.Drawing.Size(180, 29);
            this.PostAllReportProgressBar.Step = 1;
            this.PostAllReportProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.PostAllReportProgressBar.TabIndex = 5;
            this.PostAllReportProgressBar.Visible = false;
            // 
            // allReportPostingStatus
            // 
            this.allReportPostingStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.allReportPostingStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.allReportPostingStatus.Location = new System.Drawing.Point(632, 676);
            this.allReportPostingStatus.Name = "allReportPostingStatus";
            this.allReportPostingStatus.Size = new System.Drawing.Size(140, 20);
            this.allReportPostingStatus.TabIndex = 6;
            this.allReportPostingStatus.Text = "0 reports";
            this.allReportPostingStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(780, 705);
            this.Controls.Add(this.allReportPostingStatus);
            this.Controls.Add(this.PostAllReportProgressBar);
            this.Controls.Add(this.PostAllButton);
            this.Controls.Add(this.ExplanationLabel);
            this.Controls.Add(this.reportLinkTextBox);
            this.Controls.Add(this.reportLinkLabel);
            this.Controls.Add(this.MainTabControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "WoWArenaLogUploader";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.MainTabControl.ResumeLayout(false);
            this.fromLog.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsGridView)).EndInit();
            this.optionsPage.ResumeLayout(false);
            this.accountSettings.ResumeLayout(false);
            this.accountSettings.PerformLayout();
            this.fileOptions.ResumeLayout(false);
            this.fileOptions.PerformLayout();
            this.otherOptions.ResumeLayout(false);
            this.otherOptions.PerformLayout();
            this.aboutPage.ResumeLayout(false);
            this.aboutPage.PerformLayout();
            this.devPage.ResumeLayout(false);
            this.devPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage fromLog;
        private System.Windows.Forms.TabPage optionsPage;
        private System.Windows.Forms.GroupBox fileOptions;
        private System.Windows.Forms.Button changeLogLocation;
        private System.Windows.Forms.TabPage aboutPage;
        private System.Windows.Forms.TextBox gameLocationTextBox;
        private System.Windows.Forms.GroupBox otherOptions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button closeLogFile;
        private System.Windows.Forms.Button scanLogFile;
        private System.Windows.Forms.DataGridView reportsGridView;
        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.TabPage devPage;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openLogFile;
        private System.Windows.Forms.TextBox openedLogFileTextBox;
        private System.Windows.Forms.RadioButton fileScanButton;
        private System.Windows.Forms.RadioButton liveScanButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button appTokenSave;
        private System.Windows.Forms.TextBox appTokenTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label reportLinkLabel;
        private System.Windows.Forms.LinkLabel reportLinkTextBox;
        private System.Windows.Forms.Label connectionCheckLabel;
        private System.Windows.Forms.CheckBox autoOpenReportCheckbox;
        private System.Windows.Forms.GroupBox accountSettings;
        private System.Windows.Forms.Label ExplanationLabel;
        private System.Windows.Forms.CheckBox autoPostReportCheckbox;
        private System.Windows.Forms.TextBox AboutTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button PostAllButton;
        private System.Windows.Forms.ProgressBar PostAllReportProgressBar;
        private System.Windows.Forms.Label allReportPostingStatus;
    }
}

