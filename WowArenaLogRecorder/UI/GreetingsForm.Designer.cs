﻿
namespace WoWArenaLogUploader.UI
{
    partial class GreetingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GreetingsForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wowDirectoryButton = new System.Windows.Forms.Button();
            this.wowDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.continueButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label1.Location = new System.Drawing.Point(81, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(454, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to ConvokeTheSpirits.gg client application!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.label2.Location = new System.Drawing.Point(9, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(317, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Please, select your World of Warcraft directory:";
            // 
            // wowDirectoryButton
            // 
            this.wowDirectoryButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wowDirectoryButton.Location = new System.Drawing.Point(12, 113);
            this.wowDirectoryButton.Name = "wowDirectoryButton";
            this.wowDirectoryButton.Size = new System.Drawing.Size(68, 29);
            this.wowDirectoryButton.TabIndex = 2;
            this.wowDirectoryButton.Text = "Select";
            this.wowDirectoryButton.UseVisualStyleBackColor = true;
            this.wowDirectoryButton.Click += new System.EventHandler(this.wowDirectoryButton_Click);
            // 
            // wowDirectoryTextBox
            // 
            this.wowDirectoryTextBox.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.wowDirectoryTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wowDirectoryTextBox.Location = new System.Drawing.Point(86, 114);
            this.wowDirectoryTextBox.Name = "wowDirectoryTextBox";
            this.wowDirectoryTextBox.ReadOnly = true;
            this.wowDirectoryTextBox.Size = new System.Drawing.Size(530, 26);
            this.wowDirectoryTextBox.TabIndex = 3;
            // 
            // continueButton
            // 
            this.continueButton.Enabled = false;
            this.continueButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.continueButton.Location = new System.Drawing.Point(275, 160);
            this.continueButton.Margin = new System.Windows.Forms.Padding(2);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(100, 30);
            this.continueButton.TabIndex = 4;
            this.continueButton.Text = "Continue";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 209);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(634, 22);
            this.statusStrip1.TabIndex = 5;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // GreetingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(634, 231);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.continueButton);
            this.Controls.Add(this.wowDirectoryTextBox);
            this.Controls.Add(this.wowDirectoryButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GreetingsForm";
            this.Text = "WoWArenaLogUploader";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button wowDirectoryButton;
        private System.Windows.Forms.TextBox wowDirectoryTextBox;
        private System.Windows.Forms.Button continueButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    }
}