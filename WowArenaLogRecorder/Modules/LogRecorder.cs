﻿using CombatLogParser;
using CombatLogParser.LogEntries;
using CombatLogParser.LogEntries.BaseEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CombatLogParser.Localization;
using WoWArenaLogUploader.UI;

namespace WoWArenaLogUploader.Modules
{

    class LogRecorder
    {
        private Logger logger = Logger.Get();
        private Parser combatLogParser = new Parser();

        private bool isRunnig = false;

        private Dictionary<ReportKey, ReportData> reportsDict = new Dictionary<ReportKey, ReportData>();

        public delegate void OnReportFindDelegate(ReportKey key, ReportData data);

        private Delegate reportShowDelegate;

        public LogRecorder(MainForm.OnReportFindDelegate onReportFind)
        {
            reportShowDelegate = onReportFind;
        }

        public void OnReportFind(ReportKey key, ReportData data)
        {
            reportsDict.Add(key, data);
            reportShowDelegate.DynamicInvoke(key, data);
        }

        public void SetLocale(string locale)
        {
            combatLogParser.SetLocale(locale);
        }

        public void ScanFull(string directory_name)
        {
            var files = Directory.GetFiles(directory_name, SettingsManager.WOW_COMBAT_LOG_FILE_NAME + "*.txt").OrderByDescending(p => new FileInfo(p).LastWriteTime).ToArray();

            List<string> zoneIds = Enum.GetValues(typeof(ZoneID)).Cast<int>().Select(v => v.ToString()).ToList();

            foreach (var file in files)
            {
                logger.WriteLine(GetType().Name, "Open " + file);

                if (!File.Exists(file))
                    continue;

                using FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using StreamReader streamReader = new StreamReader(fileStream);

                MatchRecorder matchRecorder = new MatchRecorder(OnReportFind);

                while (!streamReader.EndOfStream)
                {
                    matchRecorder.GetLine(streamReader.ReadLine());

                    if (!isRunnig)
                    {
                        matchRecorder.End();
                        return;
                    }
                }
            }
        }

        public void ScanLive(string directory_name)
        {
            int retries = 0;

            while (true)
            {
                if (isRunnig != true)
                    break;

                logger.WriteLine(GetType().Name, "Live scan Started");

                var files = Directory.GetFiles(directory_name, SettingsManager.WOW_COMBAT_LOG_FILE_NAME + "*.txt").OrderByDescending(p => new FileInfo(p).LastWriteTime).ToArray();
                if (files.Length == 0)
                {
                    Thread.Sleep(5);
                    continue;
                }

                if (File.Exists(files[0]))
                {
                    retries = 0;

                    using FileStream fileStream = new FileStream(files[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    using StreamReader streamReader = new StreamReader(fileStream);

                    streamReader.BaseStream.Seek(0 , SeekOrigin.End);

                    MatchRecorder matchRecorder = new MatchRecorder(OnReportFind);

                    while(true)
                    {
                        var line = streamReader.ReadLine();

                        if (line == null)
                        {
                            if (isRunnig != true)
                            {
                                logger.WriteLine(GetType().Name, "live scan stopped, break!");
                                break;
                            }

                            if (retries == 60)
                            {
                                logger.WriteLine(GetType().Name, "no lines 60 secs, break!");
                                break;
                            }
                            logger.WriteLine(GetType().Name, "no line, sleeping 1 sec");
                            Thread.Sleep(1000);
                            retries++;
                            continue;
                        }
                        else
                        {
                            retries = 0;
                            matchRecorder.GetLine(line);
                        }
                    }

                }

            }

            
        }

        public void ScanFileForArenaMatches(string directory_name, bool liveScan, MainForm.OnFinishDelegate onFinish)
        {
            isRunnig = true;

            if (!liveScan)
            {
                ScanFull(directory_name);
            }
            else
            {
                ScanLive(directory_name);
            }

            onFinish();
        }

        public void RequestStop()
        {
            isRunnig = false;
        }

        public void ClearCapturedReports()
        {
            reportsDict.Clear();
        }

        public ReportData GetLogForReport(ReportKey rep_key)
        {
            ReportData rep_object;
            logger.WriteLine(GetType().Name, "trying get list for rep" + rep_key);
            if (!reportsDict.TryGetValue(rep_key, out rep_object))
            {
                logger.WriteLine(GetType().Name, "cannot find list!");
                rep_object = new ReportData(new List<BaseLogEntry>(), new Dictionary<string, LogSubject>(), new Dictionary<int, LogSpell>(), Locale.enUS);
            }
            else
            {
                logger.WriteLine(GetType().Name, "got list size " + rep_object.Events.Count);
            }

            return rep_object;
        }
    }
}
