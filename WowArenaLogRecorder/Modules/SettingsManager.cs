﻿using System;
using System.IO;
using WoWArenaLogUploader.Properties;

namespace WoWArenaLogUploader.Modules
{
    class SettingsManager
    {
        private static SettingsManager instance;
        private static readonly object settingsLock = new Object();
        private readonly Logger logger = Logger.Get();

        public static readonly string WOW_COMBAT_LOG_FILE_NAME = "WoWCombatLog";

        private static readonly string[] POSSIBLE_WOW_INSTALL_LOCATIONS =
        {
            @"C:\World of Warcraft\",
            @"C:\Games\World of Warcraft\",
            @"C:\Program Files\World of Warcraft\",
            @"C:\Program Files (x86)\World of Warcraft\",
            @"D:\World of Warcraft\",
            @"D:\Games\World of Warcraft\",
        };

        public SettingsManager()
        {
            if (!Settings.Default.IsAutoUpgraded)
            {
                logger.WriteLine(this.GetType().Name, "Settings are upgraded!");
                Settings.Default.Upgrade();
                Settings.Default.IsAutoUpgraded = true;
            }
#if DEBUG
            string path = Directory.GetCurrentDirectory();
            string report_path = path + "\\reports";
            logger.WriteLine(this.GetType().Name, "Will save reports to " + report_path);
            Settings.Default.ReportsLocation = report_path;
            Settings.Default.Save();
#endif
        }

        public static SettingsManager Get()
        {
            if (instance == null)
            {
                lock (settingsLock)
                {
                    if (instance == null)
                        instance = new SettingsManager();
                }
            }
                
            return instance;
        }

        public string GetCombatLogLocation()
        {
            return Settings.Default.WoWCombatLogLocation;
        }

        public void SetCombatLogLocation(string path)
        {
            Settings.Default.WoWCombatLogLocation = path;

            int wow_dir_index = path.LastIndexOf("\\_retail_\\Logs\\");
            if (wow_dir_index > 0)
            {
                Settings.Default.WoWLocation = path.Substring(0, wow_dir_index);
            }

            Settings.Default.Save();
        }

        public void SetGameLocation(string path)
        {
            Settings.Default.WoWLocation = path;
            Settings.Default.WoWCombatLogLocation = path + "\\_retail_\\Logs\\";
            Settings.Default.Save();
        }

        public string GetGameLocation()
        {
            return Settings.Default.WoWLocation;
        }

        public string GetReportsLocation()
        {
            return Settings.Default.ReportsLocation;
        }

        public void SetReportsLocation(string path)
        {
            Settings.Default.ReportsLocation = path;
            Settings.Default.Save();
        }

        public void SetLiveAutoPosting(bool value)
        {
            Settings.Default.AutoPostReportOnLiveScan = value;
            Settings.Default.Save();
        }

        public bool GetLiveAutoPosting()
        {
            return Settings.Default.AutoPostReportOnLiveScan;
        }

        public void SetAutoOpenReport(bool value)
        {
            Settings.Default.AutoOpenReportOnPosting = value;
            Settings.Default.Save();
        }
        public bool GetAutoOpenReport()
        {
            return Settings.Default.AutoOpenReportOnPosting;
        }

        public string GetApplicationToken()
        {
            return Settings.Default.ApplicationToken;
        }

        public void SetApplicationToken(string token)
        {
            Settings.Default.ApplicationToken = token;
            Settings.Default.Save();
        }

        private string CheckSettings()
        {
            if (Settings.Default.WoWLocation.Length <= 0)
            {
                return "Wow location is not set.";
            }
            if (!Directory.Exists(Settings.Default.WoWLocation))
            {
                return "Wow directory is not exist.";
            }

            return "Ok";
        }

        public (bool, string) StartupIsOk()
        {
            string ret_msg = CheckSettings();
            bool ret_val = false;

            if (ret_msg == "Ok")
            {
                ret_val = true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"ERROR: {ret_msg}");
            }
            return (ret_val, ret_msg);
        }
    } 
}
