﻿using System;
using System.IO;

namespace WoWArenaLogUploader.Modules
{
    class Logger
    {
        private static Logger instance;
        private static object settingsLock = new Object();

        public static Logger Get()
        {
            if (instance == null)
            {
                lock (settingsLock)
                {
                    if (instance == null)
                        instance = new Logger();
                }
            }
                
            return instance;
        }

        private Logger()
        {
        }

        [System.Diagnostics.Conditional("DEBUG")]
        private void writeLine(string path, string line)
        {
            var timeStamp = DateTime.Now.ToString("HH:mm:ss");
            System.Diagnostics.Debug.WriteLine("{0} {1} {2}", timeStamp, path, line);
        }

        public void WriteLine(string path, string line)
        {
            writeLine(path, line);
        }
    } 
}
